#!/usr/bin/env python
from __future__ import print_function

# -*- coding: utf-8 -*-
__author__ = "Gurshaant Malik, Nachiket Kapre"

import argparse
import time
from evolveNoC import pyQoR
from evolveNoC import models

parser = argparse.ArgumentParser(
    description="NoC Traffic Analyzer for Backpressure Torus"
)
parser.add_argument(
    "-N",
    metavar="size of the NxN NoC",
    type=int,
    required=True,
    help="the size of the NoC",
)
parser.add_argument(
    "-f",
    metavar="flowset file",
    type=str,
    required=True,
    help="a file containing a list of flows",
)
parser.add_argument(
    "-B",
    metavar="burst info",
    type=int,
    required=False,
    help="burst, by default reads from input file",
)
parser.add_argument(
    "-R",
    metavar="rate info",
    type=float,
    required=False,
    help="rate, by default reads from input file",
)
parser.add_argument(
    "-ML",
    metavar="The type of ML algorithm used",
    type=str,
    required=True,
    help="The type of ML algorithm you want to use. Current options are cma, rbfopt, ts and bl",
)
parser.add_argument(
    "-T",
    metavar="The amount of time each ML type is allowed to run",
    type=int,
    default=60,
    required=False,
    help="The amount of time before stopping the training. Not applicable for all-FIFO or Backpressure",
)
parser.add_argument(
    "-P",
    metavar="The population size per evolution.",
    type=int,
    required=True,
    help="The number of children generated in a population per evolution cycle",
)
parser.add_argument(
    "-W",
    metavar="Write out intermediate results explored.",
    type=int,
    default=0,
    help="Allow debugging the search space explored by the optimizer",
    required=False,
)
parser.add_argument(
    "-PF",
    metavar="Record per-flow latency for each optimizer iteration.",
    type=int,
    default=0,
    help="Allow for  debugging the search space explored by the optimizer",
    required=False,
)
parser.add_argument(
    "-O",
    metavar="Operating mode.",
    type=str,
    default="sharedlib",
    help="Allow choosing how to run the optimizer",
    required=False,
)
parser.add_argument(
    "-pss",
    metavar="Prune search space",
    type=int,
    default=0,
    help="If selected, the search space would be pruned to only \
optimise over turning hoplite routers. Non-turning routers will operate in \
fifo mode",
    required=False,
)
parser.add_argument(
    "-FF",
    metavar="The fitness function used by the optimizer",
    required=False,
    type=str,
    default="wclatency-cost",
    help="The type of ML algorithm you want to use. Current options are wclatency-cost, rate-weighted-wclatency-cost",
)
parser.add_argument(
    "-mil",
    metavar="Wether you want to minimse difference in \
sum of ideal latencies and actual latencies",
    required=False,
    type=int,
    default=0,
    help="If enabled, the optimizer will aim to minimse the difference\
between sum of latencies of flows and ideal sum of latencies of flows",
)

args = parser.parse_args()

ml_type = args.ML
max_time = args.T

def progressBar(current, total, barLength=20):
    percent = float(current) * 100 / total
    arrow = "-" * int(percent / 100 * barLength - 1) + ">"
    spaces = " " * (barLength - len(arrow))

    print("Progress: [%s%s] %d %%" % (arrow, spaces, percent), end="\r")


def learn_NoC_config(model, qor_tool):
    sample = time.time()
    stop = False
    i = 0
    while time.time() - sample < max_time and not (stop):
        i += 1
        stop = model.step(iteration=i)
        progressBar(time.time() - sample, max_time, 50)
    print("\n")
    if not (ml_type in ("bp", "fifo")):
        model.compare_QoR_against_vanilla(iteration=i + 1)
    qor_tool.close_lib()


if __name__ == "__main__":
    qor_tool = pyQoR.pyQoR(args)
    model = models.model(qor_tool, args)
    learn_NoC_config(model, qor_tool)