/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_network.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "west_fifo_network.h"
#include "any.h"
#include "mod.h"
#include "rt_nonfinite.h"
#include "west_fifo_extra_backpressure.h"
#include "west_fifo_network_data.h"
#include "west_fifo_network_emxutil.h"
#include "west_fifo_network_initialize.h"
#include <math.h>

/* Function Definitions */

/*
 * WEST_FIFO_NETWORK Computes latencies of first packet for flows in HopliteBuf with backpressure
 *
 * columns, rows: number of columns and rows in the NoC
 * burst: (rows * columns) x (rows * columns) array of burstiness of flows
 * rho: (rows * columns) x (rows * columns) array of rates of flows
 * Note: for burst and rho, the first index is the source node,
 * and the second the destination node of the flow.
 * Each node/router index is computed as: column_index + (row_index - 1) * columns
 * backpressure: (rows * columns) x 1 array of booleans.
 * False: the router uses buffer; true: the router uses backpressure.
 * method: false is time-stopping method; true is backlog-bound
 * firstPacketOnly: 1 to compute injection latency for first packet only;
 *                  0 to compute injection latency for all packets in the burst
 * returns
 * retval: true if we have a bound; false if we have no bound
 * B: (rows * columns) array of max buffer size for each router
 * Latency: (rows * columns) x (rows * columns) array of max latency for each flow
 * global variables
 * colrc (do not redefine this variable anywhere else)
 *
 * Note: burst = 0 means that the flow does not exist.
 * burst must otherwise be >=1
 * rate for a flow with burst = 0 MUST be 0
 * otherwise the rate must be between 0 and 1 (extremes excluded)
 * Arguments    : double columns
 *                double rows
 *                emxArray_real_T *burst
 *                const emxArray_real_T *rho
 *                const emxArray_real_T *backpressure
 *                double method
 *                double firstPacketOnly
 *                double *retval
 *                emxArray_real_T *B
 *                emxArray_real_T *Latency
 * Return Type  : void
 */
void west_fifo_network(double columns, double rows, emxArray_real_T *burst,
  const emxArray_real_T *rho, const emxArray_real_T *backpressure, double method,
  double firstPacketOnly, double *retval, emxArray_real_T *B, emxArray_real_T
  *Latency)
{
  double nodes;
  int b_retval;
  int loop_ub;
  int i;
  int loop_ub_tmp;
  emxArray_real_T *k;
  int has_n_to_s;
  emxArray_real_T *interrho;
  emxArray_real_T *interb;
  emxArray_real_T *burst_correction;
  emxArray_real_T *directb;
  emxArray_real_T *directrho;
  emxArray_real_T *backpressureV;
  emxArray_real_T *rhoS;
  emxArray_real_T *burstW;
  emxArray_real_T *rhoW;
  emxArray_real_T *rhoE;
  emxArray_real_T *prop;
  emxArray_real_T *totalb;
  emxArray_real_T *totalrho;
  emxArray_real_T *BV;
  emxArray_real_T *FlightLatencyW;
  emxArray_real_T *TotalLatencyS;
  emxArray_real_T *burstBack;
  emxArray_real_T *rhoBack;
  emxArray_boolean_T *b_burstW;
  int cs;
  int cd;
  unsigned int u;
  int i1;
  int rs;
  int rd;
  double firstpacket;
  int i2;
  int i3;
  int r;
  int cso;
  int has_w_to_s;
  int interrho_tmp;
  unsigned int u1;
  int i4;
  double b_cs;
  unsigned int u2;
  int exitg5;
  boolean_T stopadd;
  boolean_T exitg6;
  int exitg4;
  int exitg3;
  int exitg2;
  int exitg1;
  if (isInitialized_west_fifo_network == false) {
    west_fifo_network_initialize();
  }

  nodes = columns * rows;
  b_retval = 0;
  loop_ub = (int)nodes;
  i = B->size[0];
  B->size[0] = loop_ub;
  emxEnsureCapacity_real_T(B, i);
  for (i = 0; i < loop_ub; i++) {
    B->data[i] = 0.0;
  }

  i = Latency->size[0] * Latency->size[1];
  Latency->size[0] = loop_ub;
  Latency->size[1] = loop_ub;
  emxEnsureCapacity_real_T(Latency, i);
  loop_ub_tmp = loop_ub * loop_ub;
  for (i = 0; i < loop_ub_tmp; i++) {
    Latency->data[i] = 0.0;
  }

  emxInit_real_T(&k, 2);
  colrc = columns;
  i = k->size[0] * k->size[1];
  k->size[0] = burst->size[0];
  k->size[1] = burst->size[1];
  emxEnsureCapacity_real_T(k, i);
  has_n_to_s = burst->size[0] * burst->size[1];
  for (i = 0; i < has_n_to_s; i++) {
    k->data[i] = burst->data[i];
  }

  emxInit_real_T(&interrho, 2);
  emxInit_real_T(&interb, 2);
  emxInit_real_T(&burst_correction, 1);
  emxInit_real_T(&directb, 2);
  emxInit_real_T(&directrho, 2);
  emxInit_real_T(&backpressureV, 1);
  emxInit_real_T(&rhoS, 2);
  emxInit_real_T(&burstW, 2);
  emxInit_real_T(&rhoW, 2);
  emxInit_real_T(&rhoE, 1);
  emxInit_real_T(&prop, 2);
  emxInit_real_T(&totalb, 2);
  emxInit_real_T(&totalrho, 2);
  emxInit_real_T(&BV, 1);
  emxInit_real_T(&FlightLatencyW, 2);
  emxInit_real_T(&TotalLatencyS, 2);
  emxInit_real_T(&burstBack, 1);
  emxInit_real_T(&rhoBack, 1);
  emxInit_boolean_T(&b_burstW, 2);
  if ((!(burst->size[0] != nodes)) && (burst->size[1] == loop_ub) && (rho->size
       [0] == loop_ub) && (rho->size[1] == loop_ub) && (backpressure->size[0] ==
       loop_ub)) {
    /* note: no check is performed to ensure that burstiness and rate values  */
    /* are valid to avoid slowing down the analysis */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* GET INTERFERING HORIZONTAL FLOWS FOR PE->E FLOWS */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    i = interrho->size[0] * interrho->size[1];
    interrho->size[0] = loop_ub;
    interrho->size[1] = loop_ub;
    emxEnsureCapacity_real_T(interrho, i);
    for (i = 0; i < loop_ub_tmp; i++) {
      interrho->data[i] = 1.0;
    }

    /* remaining rate after interference - 1 means no interference */
    i = interb->size[0] * interb->size[1];
    interb->size[0] = loop_ub;
    interb->size[1] = loop_ub;
    emxEnsureCapacity_real_T(interb, i);
    for (i = 0; i < loop_ub_tmp; i++) {
      interb->data[i] = 0.0;
    }

    /* interfering burstiness - 0 means no interference */
    /* rs, cs: rows and column source; rd, cd: rows and column destination; cso, cdo: column source and destination of other flow; rdo: row destination of other flow */
    i = (int)columns;
    for (cs = 0; cs < i; cs++) {
      for (cd = 0; cd < i; cd++) {
        u = cd + 1U;
        if (cs + 1U != u) {
          /* cs == cd injects south */
          i1 = (int)rows;
          for (rs = 0; rs < i1; rs++) {
            for (rd = 0; rd < i1; rd++) {
              firstpacket = (((double)rs + 1.0) - 1.0) * colrc;
              i2 = (int)(((double)cs + 1.0) + firstpacket) - 1;
              i3 = (int)(((double)cd + 1.0) + (((double)rd + 1.0) - 1.0) * colrc)
                - 1;
              if (!(burst->data[i2 + burst->size[0] * i3] == 0.0)) {
                for (has_n_to_s = 0; has_n_to_s < i1; has_n_to_s++) {
                  /* add all flows that inject PE->S or PE->E at rs, cs other than the flow directed to rd, cd */
                  for (has_w_to_s = 0; has_w_to_s < i; has_w_to_s++) {
                    if ((has_w_to_s + 1U != u) || ((unsigned int)has_n_to_s !=
                         (unsigned int)rd)) {
                      interrho_tmp = (int)(((double)has_w_to_s + 1.0) +
                                           (((double)has_n_to_s + 1.0) - 1.0) *
                                           colrc) - 1;
                      interrho->data[i2 + interrho->size[0] * i3] -= rho->
                        data[i2 + rho->size[0] * interrho_tmp];
                      interb->data[i2 + interb->size[0] * i3] += burst->data[i2
                        + burst->size[0] * interrho_tmp];
                    }
                  }

                  /* add all flows on row rs that pass W->E at rs, cs */
                  for (cso = 0; cso < cs; cso++) {
                    r = (int)(columns + (1.0 - (((double)cs + 1.0) + 1.0)));
                    for (has_w_to_s = 0; has_w_to_s < r; has_w_to_s++) {
                      interrho_tmp = (int)(((double)cso + 1.0) + firstpacket) -
                        1;
                      loop_ub_tmp = (int)(((((double)cs + 1.0) + 1.0) + (double)
                                           has_w_to_s) + (((double)has_n_to_s +
                        1.0) - 1.0) * colrc) - 1;
                      interrho->data[i2 + interrho->size[0] * i3] -= rho->
                        data[interrho_tmp + rho->size[0] * loop_ub_tmp];
                      interb->data[i2 + interb->size[0] * i3] += burst->
                        data[interrho_tmp + burst->size[0] * loop_ub_tmp];
                    }

                    for (has_w_to_s = 0; has_w_to_s < cso; has_w_to_s++) {
                      interrho_tmp = (int)(((double)cso + 1.0) + (((double)rs +
                        1.0) - 1.0) * colrc) - 1;
                      loop_ub_tmp = (int)(((double)has_w_to_s + 1.0) + (((double)
                        has_n_to_s + 1.0) - 1.0) * colrc) - 1;
                      interrho->data[i2 + interrho->size[0] * i3] -= rho->
                        data[interrho_tmp + rho->size[0] * loop_ub_tmp];
                      interb->data[i2 + interb->size[0] * i3] += burst->
                        data[interrho_tmp + burst->size[0] * loop_ub_tmp];
                    }
                  }

                  r = (int)(columns + (1.0 - (((double)cs + 1.0) + 2.0)));
                  for (cso = 0; cso < r; cso++) {
                    nodes = (((double)cs + 1.0) + 2.0) + (double)cso;
                    i4 = (int)(unsigned int)fmod(nodes - 1.0, 4.294967296E+9) -
                      cs;
                    for (has_w_to_s = 0; has_w_to_s <= i4 - 2; has_w_to_s++) {
                      interrho_tmp = (int)(nodes + firstpacket) - 1;
                      loop_ub_tmp = (int)(((((double)cs + 1.0) + 1.0) + (double)
                                           has_w_to_s) + (((double)has_n_to_s +
                        1.0) - 1.0) * colrc) - 1;
                      interrho->data[i2 + interrho->size[0] * i3] -= rho->
                        data[interrho_tmp + rho->size[0] * loop_ub_tmp];
                      interb->data[i2 + interb->size[0] * i3] += burst->
                        data[interrho_tmp + burst->size[0] * loop_ub_tmp];
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* BURST CORRECTION COMPUTATION */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* construct burst correction matrix */
    /* burst_correction(rc(r,c) is true if r, c is a backpressure node and has a n -> s flow and a w -> s, or has a w -> e flow */
    /* that passes through a node with these two characteristics on the same row */
    /* first determine backpressure nodes with n -> s flow and w -> s flow */
    i1 = burst_correction->size[0];
    burst_correction->size[0] = loop_ub;
    emxEnsureCapacity_real_T(burst_correction, i1);
    for (i1 = 0; i1 < loop_ub; i1++) {
      burst_correction->data[i1] = 0.0;
    }

    loop_ub = (int)rows;
    for (r = 0; r < loop_ub; r++) {
      for (cso = 0; cso < i; cso++) {
        /* check if it is a backpressure node */
        firstpacket = (((double)r + 1.0) - 1.0) * colrc;
        i1 = (int)(((double)cso + 1.0) + firstpacket) - 1;
        if (!(backpressure->data[i1] == 0.0)) {
          has_n_to_s = 0;
          has_w_to_s = 0;

          /* check if there is a w -> s turn on the node (ie. there is a flow that starts on the same row r on a different col cs and */
          /* ends on a diff row rd on the same col c) */
          /* check if there is a n -> s flow on the node (ie. there is a flow that starts on a diff row rs and a not necessarily diff col cs,  */
          /* and ends on another diff row rd on the same col c, and either rs < r < rd or r < rd < rs or rd < rs < r) */
          for (rd = 0; rd < loop_ub; rd++) {
            for (cs = 0; cs < i; cs++) {
              if (((unsigned int)rd != (unsigned int)r) && ((unsigned int)cs !=
                   (unsigned int)cso) && (burst->data[((int)(((double)cs + 1.0)
                     + firstpacket) + burst->size[0] * ((int)(((double)cso + 1.0)
                      + (((double)rd + 1.0) - 1.0) * colrc) - 1)) - 1] > 0.0)) {
                has_w_to_s = 1;
              }
            }

            u = rd + 1U;
            u1 = r + 1U;
            if (u != u1) {
              for (interrho_tmp = 0; interrho_tmp < loop_ub; interrho_tmp++) {
                u2 = interrho_tmp + 1U;
                if ((u != u2) && (u2 != u1)) {
                  for (cs = 0; cs < i; cs++) {
                    if ((!(burst->data[((int)(((double)cs + 1.0) + (((double)rd
                              + 1.0) - 1.0) * colrc) + burst->size[0] * ((int)
                            (((double)cso + 1.0) + (((double)interrho_tmp + 1.0)
                              - 1.0) * colrc) - 1)) - 1] == 0.0)) && ((u >= u2) ||
                         ((u1 >= u) && (u1 <= u2))) && ((u <= u2) || (u1 >= u) ||
                         (u1 <= u2))) {
                      has_n_to_s = 1;
                    }
                  }
                }
              }
            }
          }

          if ((has_n_to_s != 0) && (has_w_to_s != 0)) {
            burst_correction->data[i1] = 1.0;
          }
        }
      }
    }

    /* propagate burst correction */
    for (r = 0; r < loop_ub; r++) {
      for (cso = 0; cso < i; cso++) {
        firstpacket = (((double)r + 1.0) - 1.0) * colrc;
        i1 = (int)(((double)cso + 1.0) + firstpacket) - 1;
        if (!(burst_correction->data[i1] > 0.0)) {
          has_n_to_s = 0;
          for (rd = 0; rd < loop_ub; rd++) {
            for (cs = 0; cs < cso; cs++) {
              i2 = (int)(columns + (1.0 - (((double)cso + 1.0) + 1.0)));
              for (cd = 0; cd < i2; cd++) {
                nodes = (((double)cso + 1.0) + 1.0) + (double)cd;
                if ((burst->data[((int)(((double)cs + 1.0) + firstpacket) +
                                  burst->size[0] * ((int)(nodes + (((double)rd +
                          1.0) - 1.0) * colrc) - 1)) - 1] > 0.0) &&
                    (burst_correction->data[(int)(nodes + firstpacket) - 1] >
                     0.0)) {
                  has_n_to_s = 1;
                }
              }

              for (cd = 0; cd < cs; cd++) {
                if ((burst->data[((int)(((double)cs + 1.0) + (((double)r + 1.0)
                        - 1.0) * colrc) + burst->size[0] * ((int)(((double)cd +
                         1.0) + (((double)rd + 1.0) - 1.0) * colrc) - 1)) - 1] >
                     0.0) && (burst_correction->data[(int)(((double)cd + 1.0) +
                      firstpacket) - 1] > 0.0)) {
                  has_n_to_s = 1;
                }
              }
            }

            i2 = (int)(columns + (1.0 - (((double)cso + 1.0) + 2.0)));
            for (cs = 0; cs < i2; cs++) {
              b_cs = (((double)cso + 1.0) + 2.0) + (double)cs;
              i3 = (int)(unsigned int)fmod(b_cs - 1.0, 4.294967296E+9) - cso;
              for (cd = 0; cd <= i3 - 2; cd++) {
                nodes = (((double)cso + 1.0) + 1.0) + (double)cd;
                if ((burst->data[((int)(b_cs + firstpacket) + burst->size[0] *
                                  ((int)(nodes + (((double)rd + 1.0) - 1.0) *
                        colrc) - 1)) - 1] > 0.0) && (burst_correction->data[(int)
                     (nodes + firstpacket) - 1] > 0.0)) {
                  has_n_to_s = 1;
                }
              }
            }
          }

          burst_correction->data[i1] = has_n_to_s;
        }
      }
    }

    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* ADD HORIZONTAL PER-HOP DELAY TO PE->E FLOWS NB, AND TOTAL PER-HOP DELAY TO B */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    for (cs = 0; cs < i; cs++) {
      for (cd = 0; cd < i; cd++) {
        u = cd + 1U;
        u1 = cs + 1U;
        if (u1 != u) {
          /* cs == cd injects south */
          for (rs = 0; rs < loop_ub; rs++) {
            for (rd = 0; rd < loop_ub; rd++) {
              firstpacket = (((double)rs + 1.0) - 1.0) * colrc;
              i1 = (int)(((double)cs + 1.0) + firstpacket) - 1;
              i2 = (int)(((double)cd + 1.0) + (((double)rd + 1.0) - 1.0) * colrc)
                - 1;
              nodes = burst->data[i1 + burst->size[0] * i2];
              if (!(nodes == 0.0)) {
                /* horizontal does not count turn router */
                if ((unsigned int)cd > (unsigned int)cs) {
                  Latency->data[i1 + Latency->size[0] * i2] = ((double)cd + 1.0)
                    - ((double)cs + 1.0);
                } else {
                  Latency->data[i1 + Latency->size[0] * i2] = (((double)cd + 1.0)
                    - ((double)cs + 1.0)) + columns;
                }

                /* for backpressured one, add vertical */
                if (backpressure->data[(int)(((double)cd + 1.0) + firstpacket) -
                    1] == 1.0) {
                  if ((unsigned int)rd > (unsigned int)rs) {
                    Latency->data[i1 + Latency->size[0] * i2] = ((Latency->
                      data[i1 + Latency->size[0] * i2] + ((double)rd + 1.0)) -
                      ((double)rs + 1.0)) + 1.0;
                  } else {
                    Latency->data[i1 + Latency->size[0] * i2] = (((Latency->
                      data[i1 + Latency->size[0] * i2] + ((double)rd + 1.0)) -
                      ((double)rs + 1.0)) + columns) + 1.0;
                  }
                }

                /* if necessary, add horizontal distance from source to dest to backpressure */
                if (u > u1) {
                  if (burst_correction->data[(int)((((double)cs + 1.0) + 1.0) +
                       firstpacket) - 1] != 0.0) {
                    burst->data[i1 + burst->size[0] * i2] = (nodes + ((double)cd
                      + 1.0)) - ((double)cs + 1.0);
                  }
                } else {
                  if (u < u1) {
                    if (((double)cs + 1.0 < columns) && (burst_correction->data
                         [(int)((((double)cs + 1.0) + 1.0) + (((double)rs + 1.0)
                           - 1.0) * colrc) - 1] != 0.0)) {
                      burst->data[i1 + burst->size[0] * i2] = ((burst->data[i1 +
                        burst->size[0] * i2] + ((double)cd + 1.0)) - ((double)cs
                        + 1.0)) + columns;
                    } else {
                      if (((double)cs + 1.0 == columns) &&
                          (burst_correction->data[(int)(firstpacket + 1.0) - 1]
                           != 0.0)) {
                        burst->data[i1 + burst->size[0] * i2] = ((burst->data[i1
                          + burst->size[0] * i2] + ((double)cd + 1.0)) -
                          ((double)cs + 1.0)) + columns;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* COMPUTE ON EACH VERTICAL LINK */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* direct backpressure interference */
    i1 = directb->size[0] * directb->size[1];
    directb->size[0] = loop_ub;
    directb->size[1] = i;
    emxEnsureCapacity_real_T(directb, i1);
    loop_ub_tmp = loop_ub * i;
    for (i1 = 0; i1 < loop_ub_tmp; i1++) {
      directb->data[i1] = 0.0;
    }

    i1 = directrho->size[0] * directrho->size[1];
    directrho->size[0] = loop_ub;
    directrho->size[1] = i;
    emxEnsureCapacity_real_T(directrho, i1);
    for (i1 = 0; i1 < loop_ub_tmp; i1++) {
      directrho->data[i1] = 0.0;
    }

    /* cv: the vertical column under consideration */
    interrho_tmp = 0;
    do {
      exitg5 = 0;
      i1 = i - 1;
      if (interrho_tmp <= i1) {
        /* %%%%%%%%%%% */
        /* START VERTICAL COLUMN */
        /* compute backpressureV, the backpressure value for the nodes on this column */
        i1 = backpressureV->size[0];
        backpressureV->size[0] = loop_ub;
        emxEnsureCapacity_real_T(backpressureV, i1);

        /* compute burstS and rhoS. These are parameters of injection PE->S at cv */
        i1 = totalb->size[0] * totalb->size[1];
        totalb->size[0] = loop_ub;
        totalb->size[1] = loop_ub;
        emxEnsureCapacity_real_T(totalb, i1);
        i1 = totalrho->size[0] * totalrho->size[1];
        totalrho->size[0] = loop_ub;
        totalrho->size[1] = loop_ub;
        emxEnsureCapacity_real_T(totalrho, i1);
        i1 = rhoS->size[0] * rhoS->size[1];
        rhoS->size[0] = loop_ub;
        rhoS->size[1] = loop_ub;
        emxEnsureCapacity_real_T(rhoS, i1);
        for (r = 0; r < loop_ub; r++) {
          has_n_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)r + 1.0) -
            1.0) * colrc) - 1;
          backpressureV->data[r] = backpressure->data[has_n_to_s];
          for (rd = 0; rd < loop_ub; rd++) {
            has_w_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)rd + 1.0)
              - 1.0) * colrc) - 1;
            totalb->data[r + totalb->size[0] * rd] = burst->data[has_n_to_s +
              burst->size[0] * has_w_to_s];
            totalrho->data[r + totalrho->size[0] * rd] = k->data[has_n_to_s +
              k->size[0] * has_w_to_s];
            rhoS->data[r + rhoS->size[0] * rd] = rho->data[has_n_to_s +
              rho->size[0] * has_w_to_s];
          }
        }

        /* compute burstW and rhoW. These are parameters of injection PE->W at cv */
        i1 = burstW->size[0] * burstW->size[1];
        burstW->size[0] = loop_ub;
        burstW->size[1] = loop_ub;
        emxEnsureCapacity_real_T(burstW, i1);
        has_n_to_s = loop_ub * loop_ub;
        i1 = rhoW->size[0] * rhoW->size[1];
        rhoW->size[0] = loop_ub;
        rhoW->size[1] = loop_ub;
        emxEnsureCapacity_real_T(rhoW, i1);
        for (i1 = 0; i1 < has_n_to_s; i1++) {
          burstW->data[i1] = 0.0;
          rhoW->data[i1] = 0.0;
        }

        for (cs = 0; cs < i; cs++) {
          if (!((double)cs + 1.0 == (double)interrho_tmp + 1.0)) {
            /* cs == cv injects south */
            for (rs = 0; rs < loop_ub; rs++) {
              for (rd = 0; rd < loop_ub; rd++) {
                has_n_to_s = (int)(((double)cs + 1.0) + (((double)rs + 1.0) -
                  1.0) * colrc) - 1;
                has_w_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)rd +
                  1.0) - 1.0) * colrc) - 1;
                burstW->data[rs + burstW->size[0] * rd] += burst->
                  data[has_n_to_s + burst->size[0] * has_w_to_s];
                rhoW->data[rs + rhoW->size[0] * rd] += rho->data[has_n_to_s +
                  rho->size[0] * has_w_to_s];
              }
            }
          }
        }

        /* compute burstE and rhoE. These are parameters of flows going PE->E at cv */
        i1 = burst_correction->size[0];
        burst_correction->size[0] = loop_ub;
        emxEnsureCapacity_real_T(burst_correction, i1);
        i1 = rhoE->size[0];
        rhoE->size[0] = loop_ub;
        emxEnsureCapacity_real_T(rhoE, i1);
        for (rs = 0; rs < loop_ub; rs++) {
          burst_correction->data[rs] = 0.0;
          rhoE->data[rs] = 0.0;
          for (rd = 0; rd < loop_ub; rd++) {
            for (cd = 0; cd < interrho_tmp; cd++) {
              has_n_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)rs +
                1.0) - 1.0) * colrc) - 1;
              has_w_to_s = (int)(((double)cd + 1.0) + (((double)rd + 1.0) - 1.0)
                                 * colrc) - 1;
              burst_correction->data[rs] += burst->data[has_n_to_s + burst->
                size[0] * has_w_to_s];
              rhoE->data[rs] += rho->data[has_n_to_s + rho->size[0] * has_w_to_s];
            }

            i1 = (int)(columns + (1.0 - (((double)interrho_tmp + 1.0) + 1.0)));
            for (cd = 0; cd < i1; cd++) {
              has_n_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)rs +
                1.0) - 1.0) * colrc) - 1;
              has_w_to_s = (int)(((((double)interrho_tmp + 1.0) + 1.0) + (double)
                                  cd) + (((double)rd + 1.0) - 1.0) * colrc) - 1;
              burst_correction->data[rs] += burst->data[has_n_to_s + burst->
                size[0] * has_w_to_s];
              rhoE->data[rs] += rho->data[has_n_to_s + rho->size[0] * has_w_to_s];
            }
          }
        }

        /* call function */
        west_fifo_extra_backpressure(burstW, rhoW, totalb, totalrho, rhoS,
          burst_correction, rhoE, backpressureV, method, firstPacketOnly,
          &stopadd, BV, FlightLatencyW, TotalLatencyS, burstBack, rhoBack);
        if (!stopadd) {
          exitg5 = 1;
        } else {
          /* set buffer sizes */
          /* set latency of PE->S nodes */
          for (r = 0; r < loop_ub; r++) {
            has_n_to_s = (int)(((double)interrho_tmp + 1.0) + (((double)r + 1.0)
              - 1.0) * colrc) - 1;
            B->data[has_n_to_s] = BV->data[r];
            for (rd = 0; rd < loop_ub; rd++) {
              Latency->data[has_n_to_s + Latency->size[0] * ((int)(((double)
                interrho_tmp + 1.0) + (((double)rd + 1.0) - 1.0) * colrc) - 1)] =
                TotalLatencyS->data[r + TotalLatencyS->size[0] * rd];
            }
          }

          /* adds latency to PE->W nodes */
          for (cs = 0; cs < i; cs++) {
            if (!((double)cs + 1.0 == (double)interrho_tmp + 1.0)) {
              /* cs == cv injects south */
              for (rs = 0; rs < loop_ub; rs++) {
                for (rd = 0; rd < loop_ub; rd++) {
                  i1 = (int)(((double)cs + 1.0) + (((double)rs + 1.0) - 1.0) *
                             colrc) - 1;
                  i2 = (int)(((double)interrho_tmp + 1.0) + (((double)rd + 1.0)
                              - 1.0) * colrc) - 1;
                  if (!(burst->data[i1 + burst->size[0] * i2] == 0.0)) {
                    Latency->data[i1 + Latency->size[0] * i2] +=
                      FlightLatencyW->data[rs + FlightLatencyW->size[0] * rd];
                  }
                }
              }
            }
          }

          /* add direct backpressure sets to any row where there is a W->S flow */
          for (r = 0; r < loop_ub; r++) {
            has_n_to_s = burstW->size[1];
            i1 = b_burstW->size[0] * b_burstW->size[1];
            b_burstW->size[0] = 1;
            b_burstW->size[1] = burstW->size[1];
            emxEnsureCapacity_boolean_T(b_burstW, i1);
            for (i1 = 0; i1 < has_n_to_s; i1++) {
              b_burstW->data[i1] = (burstW->data[r + burstW->size[0] * i1] > 0.0);
            }

            if (b_any(b_burstW)) {
              directb->data[r + directb->size[0] * interrho_tmp] =
                burstBack->data[r];
              directrho->data[r + directrho->size[0] * interrho_tmp] =
                rhoBack->data[r];
            }
          }

          /* END VERTICAL COLUMN */
          /* %%%%%%%%%%% */
          interrho_tmp++;
        }
      } else {
        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        /* INDIRECT BACKPRESSURE COMPUTATION */
        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        /* construct propagation set prop */
        /* prop(r,c) is true if the backpressure propagates from r, c+1 back to r,c */
        i2 = prop->size[0] * prop->size[1];
        prop->size[0] = loop_ub;
        prop->size[1] = i;
        emxEnsureCapacity_real_T(prop, i2);
        for (r = 0; r < loop_ub; r++) {
          for (cso = 0; cso < i; cso++) {
            /* prop(r,c) should be 1 if there is any flow that crosses the W port at both r,c+1 and r,c */
            prop->data[r + prop->size[0] * cso] = 0.0;
            for (rd = 0; rd < loop_ub; rd++) {
              for (cs = 0; cs < cso; cs++) {
                i2 = (int)(columns + (1.0 - (((double)cso + 1.0) + 1.0)));
                for (cd = 0; cd < i2; cd++) {
                  if (burst->data[((int)(((double)cs + 1.0) + (((double)r + 1.0)
                         - 1.0) * colrc) + burst->size[0] * ((int)(((((double)
                            cso + 1.0) + 1.0) + (double)cd) + (((double)rd + 1.0)
                          - 1.0) * colrc) - 1)) - 1] > 0.0) {
                    prop->data[r + prop->size[0] * cso] = 1.0;
                  }
                }

                for (cd = 0; cd < cs; cd++) {
                  if (burst->data[((int)(((double)cs + 1.0) + (((double)r + 1.0)
                         - 1.0) * colrc) + burst->size[0] * ((int)(((double)cd +
                          1.0) + (((double)rd + 1.0) - 1.0) * colrc) - 1)) - 1] >
                      0.0) {
                    prop->data[r + prop->size[0] * cso] = 1.0;
                  }
                }
              }

              i2 = (int)(columns + (1.0 - (((double)cso + 1.0) + 2.0)));
              for (cs = 0; cs < i2; cs++) {
                b_cs = (((double)cso + 1.0) + 2.0) + (double)cs;
                i3 = (int)(unsigned int)fmod(b_cs - 1.0, 4.294967296E+9) - cso;
                for (cd = 0; cd <= i3 - 2; cd++) {
                  if (burst->data[((int)(b_cs + (((double)r + 1.0) - 1.0) *
                                         colrc) + burst->size[0] * ((int)
                        (((((double)cso + 1.0) + 1.0) + (double)cd) + (((double)
                           rd + 1.0) - 1.0) * colrc) - 1)) - 1] > 0.0) {
                    prop->data[r + prop->size[0] * cso] = 1.0;
                  }
                }
              }
            }
          }
        }

        /* total backpressure is sum of direct and indirect backpressure interference */
        i2 = totalb->size[0] * totalb->size[1];
        totalb->size[0] = loop_ub;
        totalb->size[1] = i;
        emxEnsureCapacity_real_T(totalb, i2);
        for (i2 = 0; i2 < loop_ub_tmp; i2++) {
          totalb->data[i2] = 0.0;
        }

        i2 = totalrho->size[0] * totalrho->size[1];
        totalrho->size[0] = loop_ub;
        totalrho->size[1] = i;
        emxEnsureCapacity_real_T(totalrho, i2);
        for (i2 = 0; i2 < loop_ub_tmp; i2++) {
          totalrho->data[i2] = 0.0;
        }

        for (r = 0; r < loop_ub; r++) {
          for (cd = 0; cd < i; cd++) {
            /* add direct to total for the same column cd */
            totalb->data[r + totalb->size[0] * cd] += directb->data[r +
              directb->size[0] * cd];
            totalrho->data[r + totalrho->size[0] * cd] += directrho->data[r +
              directrho->size[0] * cd];

            /* iterate backward from cd, covering all other columns until it stops */
            stopadd = false;
            i2 = (int)(((-1.0 - (((double)cd + 1.0) - 1.0)) + 1.0) / -1.0);
            cso = 0;
            exitg6 = false;
            while ((!exitg6) && (cso <= i2 - 1)) {
              i3 = (cd - cso) - 1;
              if (prop->data[r + prop->size[0] * i3] != 0.0) {
                totalb->data[r + totalb->size[0] * i3] += directb->data[r +
                  directb->size[0] * cd];
                totalrho->data[r + totalrho->size[0] * i3] += directrho->data[r
                  + directrho->size[0] * cd];
                cso++;
              } else {
                stopadd = true;
                exitg6 = true;
              }
            }

            if (!stopadd) {
              i2 = (int)(((((double)cd + 1.0) + 1.0) + (-1.0 - columns)) / -1.0);
              cso = 0;
              exitg6 = false;
              while ((!exitg6) && (cso <= i2 - 1)) {
                i3 = (int)(columns + -(double)cso) - 1;
                if (prop->data[r + prop->size[0] * i3] != 0.0) {
                  totalb->data[r + totalb->size[0] * i3] += directb->data[r +
                    directb->size[0] * cd];
                  totalrho->data[r + totalrho->size[0] * i3] += directrho->
                    data[r + directrho->size[0] * cd];
                  cso++;
                } else {
                  exitg6 = true;
                }
              }
            }
          }
        }

        /* add backpressure interference rs,cs+1 to any P->E flows at rs,cs */
        for (cs = 0; cs < i; cs++) {
          for (cd = 0; cd < i; cd++) {
            if ((unsigned int)cs != (unsigned int)cd) {
              /* cs == cd injects south */
              for (rs = 0; rs < loop_ub; rs++) {
                for (rd = 0; rd < loop_ub; rd++) {
                  i2 = (int)(((double)cs + 1.0) + (((double)rs + 1.0) - 1.0) *
                             colrc) - 1;
                  i3 = (int)(((double)cd + 1.0) + (((double)rd + 1.0) - 1.0) *
                             colrc) - 1;
                  if (!(burst->data[i2 + burst->size[0] * i3] == 0.0)) {
                    interrho_tmp = (int)(b_mod((double)cs + 1.0, columns) + 1.0)
                      - 1;
                    interrho->data[i2 + interrho->size[0] * i3] -=
                      totalrho->data[rs + totalrho->size[0] * interrho_tmp];
                    interb->data[i2 + interb->size[0] * i3] += totalb->data[rs +
                      totalb->size[0] * interrho_tmp];
                  }
                }
              }
            }
          }
        }

        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        /* COMPUTE INJECTION LATENCY PE->E */
        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        cs = 0;
        exitg5 = 2;
      }
    } while (exitg5 == 0);

    if (exitg5 != 1) {
      do {
        exitg4 = 0;
        if (cs <= i1) {
          cd = 0;
          do {
            exitg3 = 0;
            if (cd <= i1) {
              if (cs + 1 == cd + 1) {
                cd++;
              } else {
                /* cs == cd injects south */
                rs = 0;
                do {
                  exitg2 = 0;
                  i = loop_ub - 1;
                  if (rs <= i) {
                    rd = 0;
                    do {
                      exitg1 = 0;
                      if (rd <= i) {
                        i2 = (int)(((double)cs + 1.0) + (((double)rs + 1.0) -
                                    1.0) * colrc) - 1;
                        i3 = (int)(((double)cd + 1.0) + (((double)rd + 1.0) -
                                    1.0) * colrc) - 1;
                        if (burst->data[i2 + burst->size[0] * i3] == 0.0) {
                          rd++;
                        } else {
                          nodes = interrho->data[i2 + interrho->size[0] * i3];
                          if (nodes <= 0.0) {
                            exitg1 = 1;
                          } else {
                            /* Note: removed this check - even if remaining rate is less than */
                            /* injection rate, latency is still correct */
                            /* if(rho(rc(rs,cs), rc(rd,cd)) > interrho(rc(rs,cs), rc(rd,cd))) */
                            /*   'remaining rate less than injection rate - cannot guarantee injection at' */
                            /*   [rs cs rd cd] */
                            /*   return */
                            /* end */
                            b_cs = rho->data[i2 + rho->size[0] * i3];

                            /* COMPUTE_INJECTION_LATENCY Computes injection latency for a flow */
                            /*  */
                            /* k, rho are the original burstiness and the rate of the flow under analysis */
                            /* confb, confrho are the burstiness and remaining rate of all conflicting flows */
                            /*  */
                            /* for a flow injected to the E port, the conflicting flows are: */
                            /* 1. any other flow injected by the same PE (to the E or to the S) */
                            /* 2. any other flow crossing the switch W->E */
                            /*  */
                            /* for a flow injected to the S port, the conflicting flows are: */
                            /* 1. any other flow injected by the same PE (to the E or to the S) */
                            /* 2. any other flow coming from N->S */
                            /* 4. any other flow turning W->S */
                            /*  */
                            /* firstPacketOnly: 1 to compute injection latency for first packet only; */
                            /*                  0 to compute injection latency for all packets in the burst */
                            firstpacket = (ceil(interb->data[i2 + interb->size[0]
                                                * i3] / nodes) - 1.0) + ceil(1.0
                              / b_cs);
                            if (!(firstPacketOnly == 1.0)) {
                              firstpacket += ceil((k->data[i2 + k->size[0] * i3]
                                                   - 1.0) * fmax(1.0 / b_cs, 1.0
                                / nodes));
                            }

                            Latency->data[i2 + Latency->size[0] * i3] +=
                              firstpacket;
                            rd++;
                          }
                        }
                      } else {
                        rs++;
                        exitg1 = 2;
                      }
                    } while (exitg1 == 0);

                    if (exitg1 == 1) {
                      exitg2 = 1;
                    }
                  } else {
                    exitg2 = 2;
                  }
                } while (exitg2 == 0);

                if (exitg2 == 1) {
                  exitg3 = 1;
                } else {
                  cd++;
                }
              }
            } else {
              cs++;
              exitg3 = 2;
            }
          } while (exitg3 == 0);

          if (exitg3 == 1) {
            exitg4 = 1;
          }
        } else {
          b_retval = 1;
          exitg4 = 1;
        }
      } while (exitg4 == 0);
    }
  }

  emxFree_boolean_T(&b_burstW);
  emxFree_real_T(&rhoBack);
  emxFree_real_T(&burstBack);
  emxFree_real_T(&TotalLatencyS);
  emxFree_real_T(&FlightLatencyW);
  emxFree_real_T(&BV);
  emxFree_real_T(&totalrho);
  emxFree_real_T(&totalb);
  emxFree_real_T(&prop);
  emxFree_real_T(&rhoE);
  emxFree_real_T(&rhoW);
  emxFree_real_T(&burstW);
  emxFree_real_T(&rhoS);
  emxFree_real_T(&backpressureV);
  emxFree_real_T(&directrho);
  emxFree_real_T(&directb);
  emxFree_real_T(&burst_correction);
  emxFree_real_T(&interb);
  emxFree_real_T(&interrho);
  emxFree_real_T(&k);
  *retval = b_retval;
}

/*
 * File trailer for west_fifo_network.c
 *
 * [EOF]
 */
