/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_extra_backpressure.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "west_fifo_extra_backpressure.h"
#include "any.h"
#include "det.h"
#include "mldivide.h"
#include "rt_nonfinite.h"
#include "sum.h"
#include "west_fifo_network.h"
#include "west_fifo_network_emxutil.h"
#include <math.h>

/* Function Definitions */

/*
 * WEST_FIFO_EXTRA_BACKPRESSURE Computes latencies for HopliteBuf vertical ring W -> S design with backpressure
 *
 * burstW: N x N array of burstiness of flows that arrive from W and turn S
 * rhoW: N x N array of rates of flows that arrive from W and turn S
 * burstS: N x N array of burst of flows that go from PE to S
 * kS: N x N array of original burst of flows that go from PE to S
 * rhoS: N x N array of rates of flows that go from PE to S
 * burstE: N x 1 vector of cumulative burstiness of flows that go from PE to E
 * rhoE: N x 1  vector of cumulative rates of flows that go from PE to E
 * for all inputs, row is entry, and column is exit
 * backpressure: N array of boolean. true means the corresponding router is backpressured
 * method: false is time-stopping method; true is backlog-bound
 * firstPacketOnly: 1 to compute injection latency for first packet only;
 *                  0 to compute injection latency for all packets in the burst
 * returns
 * retval: true if we have a bound; false if we have no bound
 * B: N array of max buffer size for each router
 * FlightLatencyW: N x N array of max in-flight latencies (buffer to exit) for flows from W to S that are not backpressured
 * TotalLatencyS: N x N array of total latencies (injection + in-flight) for flows from PE to S
 * burstBack: N array of burstiness of interfering flows for each backpressured router
 * rhoBack: N array of rates of interfering flows for each backpressured router
 *
 * Note: burst = 0 means that the flow does not exist.
 * burst must otherwise be >=1
 * rate for a flow with burst = 0 MUST be 0
 * otherwise the rate must be between 0 and 1 (extremes excluded)
 * Arguments    : const emxArray_real_T *burstW
 *                const emxArray_real_T *rhoW
 *                const emxArray_real_T *burstS
 *                const emxArray_real_T *kS
 *                const emxArray_real_T *rhoS
 *                const emxArray_real_T *burstE
 *                const emxArray_real_T *rhoE
 *                const emxArray_real_T *backpressure
 *                double method
 *                double firstPacketOnly
 *                boolean_T *retval
 *                emxArray_real_T *B
 *                emxArray_real_T *FlightLatencyW
 *                emxArray_real_T *TotalLatencyS
 *                emxArray_real_T *burstBack
 *                emxArray_real_T *rhoBack
 * Return Type  : void
 */
void west_fifo_extra_backpressure(const emxArray_real_T *burstW, const
  emxArray_real_T *rhoW, const emxArray_real_T *burstS, const emxArray_real_T
  *kS, const emxArray_real_T *rhoS, const emxArray_real_T *burstE, const
  emxArray_real_T *rhoE, const emxArray_real_T *backpressure, double method,
  double firstPacketOnly, boolean_T *retval, emxArray_real_T *B, emxArray_real_T
  *FlightLatencyW, emxArray_real_T *TotalLatencyS, emxArray_real_T *burstBack,
  emxArray_real_T *rhoBack)
{
  int i;
  int N;
  emxArray_real_T *sigmaW;
  emxArray_real_T *sigmaS;
  emxArray_real_T *sigmaWB;
  emxArray_real_T *sigmaWNB;
  emxArray_real_T *rhoWB;
  emxArray_real_T *rhoWNB;
  emxArray_real_T *R;
  emxArray_real_T *Rij;
  emxArray_real_T *sigma_l;
  emxArray_real_T *CT;
  emxArray_real_T *rho_CT;
  emxArray_real_T *rhoRouter;
  emxArray_real_T *sigmaRouter;
  emxArray_real_T *interrho;
  emxArray_int8_T *r;
  emxArray_real_T *x;
  emxArray_real_T *r1;
  emxArray_boolean_T *b_sigma_l;
  emxArray_boolean_T *b_sigmaWNB;
  int addB;
  int loop_ub_tmp;
  int b_i;
  boolean_T guard1 = false;
  int p;
  int l;
  unsigned int b_p;
  int idx;
  double y;
  int i1;
  int n;
  double sigmaMax;
  double rhoMax;
  boolean_T exitg6;
  double b_y;
  unsigned int b_l;
  int exitg2;
  int exitg1;
  int exitg5;
  int exitg4;
  int exitg3;
  *retval = false;
  i = FlightLatencyW->size[0] * FlightLatencyW->size[1];
  FlightLatencyW->size[0] = 1;
  FlightLatencyW->size[1] = 1;
  emxEnsureCapacity_real_T(FlightLatencyW, i);
  FlightLatencyW->data[0] = 0.0;
  i = TotalLatencyS->size[0] * TotalLatencyS->size[1];
  TotalLatencyS->size[0] = 1;
  TotalLatencyS->size[1] = 1;
  emxEnsureCapacity_real_T(TotalLatencyS, i);
  TotalLatencyS->data[0] = 0.0;
  i = B->size[0];
  B->size[0] = 1;
  emxEnsureCapacity_real_T(B, i);
  B->data[0] = 0.0;
  i = burstBack->size[0];
  burstBack->size[0] = 1;
  emxEnsureCapacity_real_T(burstBack, i);
  burstBack->data[0] = 0.0;
  i = rhoBack->size[0];
  rhoBack->size[0] = 1;
  emxEnsureCapacity_real_T(rhoBack, i);
  rhoBack->data[0] = 0.0;
  N = burstW->size[0];
  emxInit_real_T(&sigmaW, 2);
  emxInit_real_T(&sigmaS, 2);
  emxInit_real_T(&sigmaWB, 2);
  emxInit_real_T(&sigmaWNB, 2);
  emxInit_real_T(&rhoWB, 2);
  emxInit_real_T(&rhoWNB, 2);
  emxInit_real_T(&R, 1);
  emxInit_real_T(&Rij, 2);
  emxInit_real_T(&sigma_l, 1);
  emxInit_real_T(&CT, 1);
  emxInit_real_T(&rho_CT, 1);
  emxInit_real_T(&rhoRouter, 1);
  emxInit_real_T(&sigmaRouter, 1);
  emxInit_real_T(&interrho, 1);
  emxInit_int8_T(&r, 2);
  emxInit_real_T(&x, 2);
  emxInit_real_T(&r1, 2);
  emxInit_boolean_T(&b_sigma_l, 1);
  emxInit_boolean_T(&b_sigmaWNB, 2);
  if ((N == burstW->size[1]) && (N == rhoW->size[0]) && (N == rhoW->size[1]) &&
      (N == burstS->size[0]) && (N == burstS->size[1]) && (N == rhoS->size[0]) &&
      (N == rhoS->size[1]) && (N == burstE->size[0]) && (N == rhoE->size[0]) &&
      (N == backpressure->size[0])) {
    /* note: no check is performed to ensure that burstiness and rate values  */
    /* are valid to avoid slowing down the analysis */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    /* GENERAL STRUCTURES */
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
    i = sigmaW->size[0] * sigmaW->size[1];
    sigmaW->size[0] = burstW->size[0];
    sigmaW->size[1] = burstW->size[1];
    emxEnsureCapacity_real_T(sigmaW, i);
    addB = burstW->size[0] * burstW->size[1];
    for (i = 0; i < addB; i++) {
      sigmaW->data[i] = burstW->data[i] - rhoW->data[i];
    }

    i = sigmaS->size[0] * sigmaS->size[1];
    sigmaS->size[0] = burstS->size[0];
    sigmaS->size[1] = burstS->size[1];
    emxEnsureCapacity_real_T(sigmaS, i);
    addB = burstS->size[0] * burstS->size[1];
    for (i = 0; i < addB; i++) {
      sigmaS->data[i] = burstS->data[i] - rhoS->data[i];
    }

    /* backpressure structures */
    i = sigmaWB->size[0] * sigmaWB->size[1];
    sigmaWB->size[0] = N;
    sigmaWB->size[1] = N;
    emxEnsureCapacity_real_T(sigmaWB, i);
    loop_ub_tmp = N * N;
    for (i = 0; i < loop_ub_tmp; i++) {
      sigmaWB->data[i] = 0.0;
    }

    i = sigmaWNB->size[0] * sigmaWNB->size[1];
    sigmaWNB->size[0] = N;
    sigmaWNB->size[1] = N;
    emxEnsureCapacity_real_T(sigmaWNB, i);
    for (i = 0; i < loop_ub_tmp; i++) {
      sigmaWNB->data[i] = 0.0;
    }

    i = rhoWNB->size[0] * rhoWNB->size[1];
    rhoWNB->size[0] = N;
    rhoWNB->size[1] = N;
    emxEnsureCapacity_real_T(rhoWNB, i);
    for (i = 0; i < loop_ub_tmp; i++) {
      rhoWNB->data[i] = 0.0;
    }

    for (b_i = 0; b_i < N; b_i++) {
      if (backpressure->data[b_i] == 0.0) {
        addB = sigmaW->size[1];
        for (i = 0; i < addB; i++) {
          sigmaWNB->data[b_i + sigmaWNB->size[0] * i] = sigmaW->data[b_i +
            sigmaW->size[0] * i];
        }

        addB = rhoW->size[1];
        for (i = 0; i < addB; i++) {
          rhoWNB->data[b_i + rhoWNB->size[0] * i] = rhoW->data[b_i + rhoW->size
            [0] * i];
        }
      } else {
        addB = sigmaW->size[1];
        for (i = 0; i < addB; i++) {
          sigmaWB->data[b_i + sigmaWB->size[0] * i] = sigmaW->data[b_i +
            sigmaW->size[0] * i];
        }
      }
    }

    i = R->size[0];
    R->size[0] = N;
    emxEnsureCapacity_real_T(R, i);

    /* this term captures R(i), remaining bandwidth at node i after subtracting N->S */
    for (b_i = 0; b_i < N; b_i++) {
      R->data[b_i] = 1.0;
      for (p = 0; p < b_i; p++) {
        i = N - b_i;
        for (l = 0; l < i; l++) {
          idx = (int)((unsigned int)b_i + l);
          R->data[b_i] = (R->data[b_i] - rhoW->data[p + rhoW->size[0] * idx]) -
            rhoS->data[p + rhoS->size[0] * idx];
        }

        for (l = 0; l < p; l++) {
          R->data[b_i] = (R->data[b_i] - rhoW->data[p + rhoW->size[0] * l]) -
            rhoS->data[p + rhoS->size[0] * l];
        }
      }

      i = N - b_i;
      for (p = 0; p <= i - 2; p++) {
        b_p = ((unsigned int)b_i + p) + 2U;
        y = fmod((double)b_p - 1.0, 4.294967296E+9);
        if (y < 0.0) {
          l = -(int)(unsigned int)-y;
        } else {
          l = (int)(unsigned int)y;
        }

        i1 = l - b_i;
        for (l = 0; l < i1; l++) {
          idx = (int)b_p - 1;
          n = (int)((unsigned int)b_i + l);
          R->data[b_i] = (R->data[b_i] - rhoW->data[idx + rhoW->size[0] * n]) -
            rhoS->data[idx + rhoS->size[0] * n];
        }
      }
    }

    guard1 = false;
    if (method == 0.0) {
      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
      /* TIME STOPPING BEGINS */
      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
      /* NOTE: we are trying to find a bound for flows W->S NB after leaving the buffer. */
      /* The flows W->S B are treated like flows PE->S for such computations. */
      i = Rij->size[0] * Rij->size[1];
      Rij->size[0] = N;
      Rij->size[1] = N;
      emxEnsureCapacity_real_T(Rij, i);

      /* this term captures R(i,j), remaining bandwidth for flow i->j */
      for (b_i = 0; b_i < N; b_i++) {
        for (n = 0; n < N; n++) {
          addB = rhoWNB->size[1];
          i = x->size[0] * x->size[1];
          x->size[0] = 1;
          x->size[1] = rhoWNB->size[1];
          emxEnsureCapacity_real_T(x, i);
          for (i = 0; i < addB; i++) {
            x->data[i] = rhoWNB->data[b_i + rhoWNB->size[0] * i];
          }

          i = rhoWNB->size[1];
          if (rhoWNB->size[1] == 0) {
            b_y = 0.0;
          } else {
            b_y = rhoWNB->data[b_i];
            for (addB = 2; addB <= i; addB++) {
              b_y += x->data[addB - 1];
            }
          }

          Rij->data[b_i + Rij->size[0] * n] = (R->data[b_i] - b_y) +
            rhoWNB->data[b_i + rhoWNB->size[0] * n];
        }
      }

      i = sigma_l->size[0];
      sigmaMax = (double)N * (double)N;
      addB = (int)sigmaMax;
      sigma_l->size[0] = (int)sigmaMax;
      emxEnsureCapacity_real_T(sigma_l, i);
      for (i = 0; i < addB; i++) {
        sigma_l->data[i] = 0.0;
      }

      /* Linearize the sigmaWNB into a vector of size N*N */
      for (b_i = 0; b_i < N; b_i++) {
        for (n = 0; n < N; n++) {
          sigma_l->data[(int)((((double)b_i + 1.0) * (double)N + ((double)n +
            1.0)) - (double)N) - 1] = sigmaWNB->data[b_i + sigmaWNB->size[0] * n];
        }
      }

      i = sigmaW->size[0] * sigmaW->size[1];
      sigmaW->size[0] = N;
      sigmaW->size[1] = (int)sigmaMax;
      emxEnsureCapacity_real_T(sigmaW, i);
      idx = N * (int)sigmaMax;
      for (i = 0; i < idx; i++) {
        sigmaW->data[i] = 0.0;
      }

      i = rhoRouter->size[0];
      rhoRouter->size[0] = N;
      emxEnsureCapacity_real_T(rhoRouter, i);

      /* these two terms captures T(i), the service delay at node i.  */
      /* Extra(i) is the costant term depending on S flows */
      /* Apartial is a multiplication matrix to solve the system of sigma' */
      for (b_i = 0; b_i < N; b_i++) {
        rhoRouter->data[b_i] = 0.0;
        for (p = 0; p < b_i; p++) {
          i = N - b_i;
          for (l = 0; l < i; l++) {
            b_l = ((unsigned int)b_i + l) + 1U;
            sigmaW->data[b_i + sigmaW->size[0] * ((int)((((double)p + 1.0) *
              (double)N + (double)b_l) - (double)N) - 1)] = 1.0 / R->data[b_i];
            idx = (int)b_l - 1;
            rhoRouter->data[b_i] += (sigmaS->data[p + sigmaS->size[0] * idx] +
              sigmaWB->data[p + sigmaWB->size[0] * idx]) / R->data[b_i];
          }

          for (l = 0; l < p; l++) {
            sigmaW->data[b_i + sigmaW->size[0] * ((int)((((double)p + 1.0) *
              (double)N + ((double)l + 1.0)) - (double)N) - 1)] = 1.0 / R->
              data[b_i];
            rhoRouter->data[b_i] += (sigmaS->data[p + sigmaS->size[0] * l] +
              sigmaWB->data[p + sigmaWB->size[0] * l]) / R->data[b_i];
          }
        }

        i = N - b_i;
        for (p = 0; p <= i - 2; p++) {
          b_p = ((unsigned int)b_i + p) + 2U;
          y = fmod((double)b_p - 1.0, 4.294967296E+9);
          if (y < 0.0) {
            l = -(int)(unsigned int)-y;
          } else {
            l = (int)(unsigned int)y;
          }

          i1 = l - b_i;
          for (l = 0; l < i1; l++) {
            b_l = ((unsigned int)b_i + l) + 1U;
            sigmaW->data[b_i + sigmaW->size[0] * ((int)(((double)b_p * (double)N
              + (double)b_l) - (double)N) - 1)] = 1.0 / R->data[b_i];
            idx = (int)b_p - 1;
            n = (int)b_l - 1;
            rhoRouter->data[b_i] += (sigmaS->data[idx + sigmaS->size[0] * n] +
              sigmaWB->data[idx + sigmaWB->size[0] * n]) / R->data[b_i];
          }
        }
      }

      i = rhoWB->size[0] * rhoWB->size[1];
      rhoWB->size[0] = (int)sigmaMax;
      rhoWB->size[1] = (int)sigmaMax;
      emxEnsureCapacity_real_T(rhoWB, i);
      idx = (int)sigmaMax * (int)sigmaMax;
      for (i = 0; i < idx; i++) {
        rhoWB->data[i] = 0.0;
      }

      /* full multiplication matrix to solve the sigma' system;  */
      /* note T(i) must be multiplied by rhoWNB(i,j) for each flow under analysis */
      for (b_i = 0; b_i < N; b_i++) {
        for (n = 0; n < N; n++) {
          i = (int)((((double)b_i + 1.0) * (double)N + ((double)n + 1.0)) -
                    (double)N) - 1;
          rhoMax = rhoWNB->data[b_i + rhoWNB->size[0] * n];
          idx = sigmaW->size[1];
          for (i1 = 0; i1 < idx; i1++) {
            rhoWB->data[i + rhoWB->size[0] * i1] = sigmaW->data[b_i +
              sigmaW->size[0] * i1] * rhoMax;
          }
        }
      }

      i = sigmaRouter->size[0];
      sigmaRouter->size[0] = N;
      emxEnsureCapacity_real_T(sigmaRouter, i);

      /* contains the constant terms sum_{l} sigma(i,l) / R(i) in T(i,j) */
      /* in other words, this are WNB flows entering at i */
      /* note we also sum the one under analysis; this will be taken out in CT */
      for (b_i = 0; b_i < N; b_i++) {
        sigmaRouter->data[b_i] = 0.0;
        for (l = 0; l < N; l++) {
          sigmaRouter->data[b_i] += sigmaWNB->data[b_i + sigmaWNB->size[0] * l] /
            R->data[b_i];
        }
      }

      i = CT->size[0];
      CT->size[0] = (int)sigmaMax;
      emxEnsureCapacity_real_T(CT, i);
      for (i = 0; i < addB; i++) {
        CT->data[i] = 0.0;
      }

      i = rho_CT->size[0];
      rho_CT->size[0] = (int)sigmaMax;
      emxEnsureCapacity_real_T(rho_CT, i);
      for (i = 0; i < addB; i++) {
        rho_CT->data[i] = 0.0;
      }

      /* all constant terms that end in T(i,j) */
      /* we use Extra(i) + CTpartial(i), where we have to take out the f.u.a. */
      /* rho_CT is then scaled by rho(i,j) */
      for (b_i = 0; b_i < N; b_i++) {
        for (n = 0; n < N; n++) {
          i = (int)((((double)b_i + 1.0) * (double)N + ((double)n + 1.0)) -
                    (double)N) - 1;
          CT->data[i] = (sigmaRouter->data[b_i] - sigmaWNB->data[b_i +
                         sigmaWNB->size[0] * n] / R->data[b_i]) +
            rhoRouter->data[b_i];
          rho_CT->data[i] = rhoWNB->data[b_i + rhoWNB->size[0] * n] * CT->data[i];
        }
      }

      /* can we invert? NOTE: BEWARE OF NUMERICAL ERRORS */
      idx = (int)sigmaMax;
      i = r->size[0] * r->size[1];
      r->size[0] = idx;
      r->size[1] = idx;
      emxEnsureCapacity_int8_T(r, i);
      addB = idx * idx;
      for (i = 0; i < addB; i++) {
        r->data[i] = 0;
      }

      if (idx > 0) {
        for (addB = 0; addB < idx; addB++) {
          r->data[addB + r->size[0] * addB] = 1;
        }
      }

      i = r1->size[0] * r1->size[1];
      r1->size[0] = r->size[0];
      r1->size[1] = r->size[1];
      emxEnsureCapacity_real_T(r1, i);
      addB = r->size[0] * r->size[1];
      for (i = 0; i < addB; i++) {
        r1->data[i] = (double)r->data[i] - rhoWB->data[i];
      }

      if (!(det(r1) == 0.0)) {
        /* compute result for S flows in linearized array */
        addB = sigma_l->size[0];
        for (i = 0; i < addB; i++) {
          sigma_l->data[i] += rho_CT->data[i];
        }

        i = r1->size[0] * r1->size[1];
        r1->size[0] = r->size[0];
        r1->size[1] = r->size[1];
        emxEnsureCapacity_real_T(r1, i);
        addB = r->size[0] * r->size[1];
        for (i = 0; i < addB; i++) {
          r1->data[i] = (double)r->data[i] - rhoWB->data[i];
        }

        mldivide(r1, sigma_l);

        /* can't have negative burstiness, this indicates infeasibility... */
        /*  Nachiket added negative epsilon to avoid numerical issues with matrix */
        /*  inversion */
        i = b_sigma_l->size[0];
        b_sigma_l->size[0] = sigma_l->size[0];
        emxEnsureCapacity_boolean_T(b_sigma_l, i);
        addB = sigma_l->size[0];
        for (i = 0; i < addB; i++) {
          b_sigma_l->data[i] = (sigma_l->data[i] < -1.0E-9);
        }

        if (!any(b_sigma_l)) {
          /* determine Latency for west flows NB */
          /* delay is sigma(i,j) / R(i,j) + T(i,j)  */
          /* for latency add hop distance */
          i = FlightLatencyW->size[0] * FlightLatencyW->size[1];
          FlightLatencyW->size[0] = N;
          FlightLatencyW->size[1] = N;
          emxEnsureCapacity_real_T(FlightLatencyW, i);
          for (i = 0; i < loop_ub_tmp; i++) {
            FlightLatencyW->data[i] = 0.0;
          }

          b_i = 0;
          do {
            exitg5 = 0;
            if (b_i <= N - 1) {
              n = 0;
              do {
                exitg4 = 0;
                if (n <= N - 1) {
                  if (sigmaWNB->data[b_i + sigmaWNB->size[0] * n] == 0.0) {
                    FlightLatencyW->data[b_i + FlightLatencyW->size[0] * n] =
                      0.0;
                    n++;
                  } else if (rhoWNB->data[b_i + rhoWNB->size[0] * n] > Rij->
                             data[b_i + Rij->size[0] * n]) {
                    exitg4 = 1;
                  } else {
                    if ((sigmaW->size[1] == 1) || (sigma_l->size[0] == 1)) {
                      addB = sigmaW->size[1];
                      rhoMax = 0.0;
                      for (i = 0; i < addB; i++) {
                        rhoMax += sigmaW->data[b_i + sigmaW->size[0] * i] *
                          sigma_l->data[i];
                      }
                    } else {
                      addB = sigmaW->size[1];
                      rhoMax = 0.0;
                      for (i = 0; i < addB; i++) {
                        rhoMax += sigmaW->data[b_i + sigmaW->size[0] * i] *
                          sigma_l->data[i];
                      }
                    }

                    if (n + 1 >= b_i + 1) {
                      y = (double)(n - b_i) + 1.0;
                    } else {
                      y = ((double)(n - b_i) + 1.0) + (double)N;
                    }

                    FlightLatencyW->data[b_i + FlightLatencyW->size[0] * n] =
                      (sigmaWNB->data[b_i + sigmaWNB->size[0] * n] / Rij->
                       data[b_i + Rij->size[0] * n] + (rhoMax + CT->data[(int)
                        ((((double)b_i + 1.0) * (double)N + ((double)n + 1.0)) -
                         (double)N) - 1])) + y;
                    n++;
                  }
                } else {
                  b_i++;
                  exitg4 = 2;
                }
              } while (exitg4 == 0);

              if (exitg4 == 1) {
                exitg5 = 1;
              }
            } else {
              /* floor the latencies  */
              idx = FlightLatencyW->size[0] * FlightLatencyW->size[1];
              for (addB = 0; addB < idx; addB++) {
                FlightLatencyW->data[addB] = floor(FlightLatencyW->data[addB]);
              }

              /* compute buffer size */
              /* buffer size is sum of sigma of W flows entering + sum of rho * T(i) */
              i = B->size[0];
              B->size[0] = N;
              emxEnsureCapacity_real_T(B, i);
              for (i = 0; i < N; i++) {
                B->data[i] = 0.0;
              }

              b_i = 0;
              exitg5 = 2;
            }
          } while (exitg5 == 0);

          if (exitg5 != 1) {
            do {
              exitg3 = 0;
              if (b_i <= N - 1) {
                addB = sigmaWNB->size[1];
                i = x->size[0] * x->size[1];
                x->size[0] = 1;
                x->size[1] = sigmaWNB->size[1];
                emxEnsureCapacity_real_T(x, i);
                for (i = 0; i < addB; i++) {
                  x->data[i] = sigmaWNB->data[b_i + sigmaWNB->size[0] * i];
                }

                i = sigmaWNB->size[1];
                if (sigmaWNB->size[1] == 0) {
                  b_y = 0.0;
                } else {
                  b_y = sigmaWNB->data[b_i];
                  for (addB = 2; addB <= i; addB++) {
                    b_y += x->data[addB - 1];
                  }
                }

                if (b_y == 0.0) {
                  B->data[b_i] = 0.0;
                  b_i++;
                } else {
                  addB = rhoWNB->size[1];
                  i = x->size[0] * x->size[1];
                  x->size[0] = 1;
                  x->size[1] = rhoWNB->size[1];
                  emxEnsureCapacity_real_T(x, i);
                  for (i = 0; i < addB; i++) {
                    x->data[i] = rhoWNB->data[b_i + rhoWNB->size[0] * i];
                  }

                  i = rhoWNB->size[1];
                  if (rhoWNB->size[1] == 0) {
                    b_y = 0.0;
                  } else {
                    b_y = rhoWNB->data[b_i];
                    for (addB = 2; addB <= i; addB++) {
                      b_y += x->data[addB - 1];
                    }
                  }

                  if (b_y > R->data[b_i]) {
                    exitg3 = 1;
                  } else {
                    if ((sigmaW->size[1] == 1) || (sigma_l->size[0] == 1)) {
                      addB = sigmaW->size[1];
                      rhoMax = 0.0;
                      for (i = 0; i < addB; i++) {
                        rhoMax += sigmaW->data[b_i + sigmaW->size[0] * i] *
                          sigma_l->data[i];
                      }
                    } else {
                      addB = sigmaW->size[1];
                      rhoMax = 0.0;
                      for (i = 0; i < addB; i++) {
                        rhoMax += sigmaW->data[b_i + sigmaW->size[0] * i] *
                          sigma_l->data[i];
                      }
                    }

                    addB = sigmaWNB->size[1];
                    i = x->size[0] * x->size[1];
                    x->size[0] = 1;
                    x->size[1] = sigmaWNB->size[1];
                    emxEnsureCapacity_real_T(x, i);
                    for (i = 0; i < addB; i++) {
                      x->data[i] = sigmaWNB->data[b_i + sigmaWNB->size[0] * i];
                    }

                    i = sigmaWNB->size[1];
                    if (sigmaWNB->size[1] == 0) {
                      b_y = 0.0;
                    } else {
                      b_y = sigmaWNB->data[b_i];
                      for (addB = 2; addB <= i; addB++) {
                        b_y += x->data[addB - 1];
                      }
                    }

                    addB = rhoWNB->size[1];
                    i = x->size[0] * x->size[1];
                    x->size[0] = 1;
                    x->size[1] = rhoWNB->size[1];
                    emxEnsureCapacity_real_T(x, i);
                    for (i = 0; i < addB; i++) {
                      x->data[i] = rhoWNB->data[b_i + rhoWNB->size[0] * i];
                    }

                    i = rhoWNB->size[1];
                    if (rhoWNB->size[1] == 0) {
                      y = 0.0;
                    } else {
                      y = rhoWNB->data[b_i];
                      for (addB = 2; addB <= i; addB++) {
                        y += x->data[addB - 1];
                      }
                    }

                    B->data[b_i] = b_y + (rhoMax + rhoRouter->data[b_i]) * y;
                    b_i++;
                  }
                }
              } else {
                /* floor buffer sizes, since it has to be integer */
                idx = B->size[0];
                for (addB = 0; addB < idx; addB++) {
                  B->data[addB] = floor(B->data[addB]);
                }

                /* BACKPRESSURE FLOWS AND INTERFERENCE ON PE->S */
                /* backpressuring flows. These are the N->S on each backpressured router */
                i = rhoBack->size[0];
                rhoBack->size[0] = N;
                emxEnsureCapacity_real_T(rhoBack, i);
                for (i = 0; i < N; i++) {
                  rhoBack->data[i] = 0.0;
                }

                i = burstBack->size[0];
                burstBack->size[0] = N;
                emxEnsureCapacity_real_T(burstBack, i);
                for (b_i = 0; b_i < N; b_i++) {
                  burstBack->data[b_i] = 0.0;
                  if (!(backpressure->data[b_i] == 0.0)) {
                    rhoMax = 0.0;
                    for (p = 0; p < b_i; p++) {
                      i = N - b_i;
                      if (0 <= i - 1) {
                        rhoBack->data[b_i] = 1.0 - R->data[b_i];
                      }

                      for (l = 0; l < i; l++) {
                        b_l = ((unsigned int)b_i + l) + 1U;
                        idx = (int)b_l - 1;
                        rhoMax = ((rhoMax + sigma_l->data[(int)((((double)p +
                          1.0) * (double)N + (double)b_l) - (double)N) - 1]) +
                                  sigmaWB->data[p + sigmaWB->size[0] * idx]) +
                          sigmaS->data[p + sigmaS->size[0] * idx];
                      }

                      if (0 <= p - 1) {
                        rhoBack->data[b_i] = 1.0 - R->data[b_i];
                      }

                      for (l = 0; l < p; l++) {
                        rhoMax = ((rhoMax + sigma_l->data[(int)((((double)p +
                          1.0) * (double)N + ((double)l + 1.0)) - (double)N) - 1])
                                  + sigmaWB->data[p + sigmaWB->size[0] * l]) +
                          sigmaS->data[p + sigmaS->size[0] * l];
                      }
                    }

                    i = N - b_i;
                    for (p = 0; p <= i - 2; p++) {
                      b_p = ((unsigned int)b_i + p) + 2U;
                      y = fmod((double)b_p - 1.0, 4.294967296E+9);
                      if (y < 0.0) {
                        l = -(int)(unsigned int)-y;
                      } else {
                        l = (int)(unsigned int)y;
                      }

                      i1 = l - b_i;
                      if (0 <= i1 - 1) {
                        rhoBack->data[b_i] = 1.0 - R->data[b_i];
                      }

                      for (l = 0; l < i1; l++) {
                        b_l = ((unsigned int)b_i + l) + 1U;
                        idx = (int)b_p - 1;
                        n = (int)b_l - 1;
                        rhoMax = ((rhoMax + sigma_l->data[(int)(((double)b_p *
                          (double)N + (double)b_l) - (double)N) - 1]) +
                                  sigmaWB->data[idx + sigmaWB->size[0] * n]) +
                          sigmaS->data[idx + sigmaS->size[0] * n];
                      }
                    }

                    if ((rhoMax > 0.0) || (rhoBack->data[b_i] > 0.0)) {
                      burstBack->data[b_i] = ceil((rhoMax + 1.0) + rhoBack->
                        data[b_i]);
                    }
                  }
                }

                /* compute the interfering sigma from N->S and W->S on S port of each node i */
                /* Here we consider both W->S B and NB */
                i = rhoRouter->size[0];
                rhoRouter->size[0] = N;
                emxEnsureCapacity_real_T(rhoRouter, i);
                for (i = 0; i < N; i++) {
                  rhoRouter->data[i] = 0.0;
                }

                for (b_i = 0; b_i < N; b_i++) {
                  /* W->S */
                  y = ((double)b_i + 1.0) * (double)N;
                  rhoMax = (y + 1.0) - (double)N;
                  if (rhoMax > y) {
                    i = -1;
                    i1 = -1;
                  } else {
                    i = (int)rhoMax - 2;
                    i1 = (int)y - 1;
                  }

                  idx = i1 - i;
                  if (idx == 0) {
                    b_y = 0.0;
                  } else {
                    b_y = sigma_l->data[i + 1];
                    for (addB = 2; addB <= idx; addB++) {
                      b_y += sigma_l->data[i + addB];
                    }
                  }

                  addB = sigmaWB->size[1];
                  i = x->size[0] * x->size[1];
                  x->size[0] = 1;
                  x->size[1] = sigmaWB->size[1];
                  emxEnsureCapacity_real_T(x, i);
                  for (i = 0; i < addB; i++) {
                    x->data[i] = sigmaWB->data[b_i + sigmaWB->size[0] * i];
                  }

                  i = sigmaWB->size[1];
                  if (sigmaWB->size[1] == 0) {
                    y = 0.0;
                  } else {
                    y = sigmaWB->data[b_i];
                    for (addB = 2; addB <= i; addB++) {
                      y += x->data[addB - 1];
                    }
                  }

                  rhoRouter->data[b_i] = (rhoRouter->data[b_i] + b_y) + y;

                  /* N->S */
                  for (p = 0; p < b_i; p++) {
                    i = N - b_i;
                    for (l = 0; l < i; l++) {
                      b_l = ((unsigned int)b_i + l) + 1U;
                      idx = (int)b_l - 1;
                      rhoRouter->data[b_i] = ((rhoRouter->data[b_i] +
                        sigma_l->data[(int)((((double)p + 1.0) * (double)N +
                        (double)b_l) - (double)N) - 1]) + sigmaS->data[p +
                        sigmaS->size[0] * idx]) + sigmaWB->data[p +
                        sigmaWB->size[0] * idx];
                    }

                    for (l = 0; l < p; l++) {
                      rhoRouter->data[b_i] = ((rhoRouter->data[b_i] +
                        sigma_l->data[(int)((((double)p + 1.0) * (double)N +
                        ((double)l + 1.0)) - (double)N) - 1]) + sigmaS->data[p +
                        sigmaS->size[0] * l]) + sigmaWB->data[p + sigmaWB->size
                        [0] * l];
                    }
                  }

                  i = N - b_i;
                  for (p = 0; p <= i - 2; p++) {
                    b_p = ((unsigned int)b_i + p) + 2U;
                    y = fmod((double)b_p - 1.0, 4.294967296E+9);
                    if (y < 0.0) {
                      l = -(int)(unsigned int)-y;
                    } else {
                      l = (int)(unsigned int)y;
                    }

                    i1 = l - b_i;
                    for (l = 0; l < i1; l++) {
                      b_l = ((unsigned int)b_i + l) + 1U;
                      idx = (int)b_p - 1;
                      n = (int)b_l - 1;
                      rhoRouter->data[b_i] = ((rhoRouter->data[b_i] +
                        sigma_l->data[(int)(((double)b_p * (double)N + (double)
                        b_l) - (double)N) - 1]) + sigmaS->data[idx +
                        sigmaS->size[0] * n]) + sigmaWB->data[idx +
                        sigmaWB->size[0] * n];
                    }
                  }
                }

                /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
                /* TIME STOPPING ENDS */
                /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
                exitg3 = 2;
              }
            } while (exitg3 == 0);

            if (exitg3 != 1) {
              guard1 = true;
            }
          }
        }
      }
    } else {
      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
      /* BUFFER BOUND BEGINS */
      /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
      i = rhoRouter->size[0];
      rhoRouter->size[0] = N;
      emxEnsureCapacity_real_T(rhoRouter, i);
      for (i = 0; i < N; i++) {
        rhoRouter->data[i] = 0.0;
      }

      i = sigmaRouter->size[0];
      sigmaRouter->size[0] = N;
      emxEnsureCapacity_real_T(sigmaRouter, i);
      for (i = 0; i < N; i++) {
        sigmaRouter->data[i] = 0.0;
      }

      /* these terms capture sum of rho and sigma at every router, for W->S NB and N->S */
      for (b_i = 0; b_i < N; b_i++) {
        addB = rhoWNB->size[1];
        i = x->size[0] * x->size[1];
        x->size[0] = 1;
        x->size[1] = rhoWNB->size[1];
        emxEnsureCapacity_real_T(x, i);
        for (i = 0; i < addB; i++) {
          x->data[i] = rhoWNB->data[b_i + rhoWNB->size[0] * i];
        }

        i = rhoWNB->size[1];
        if (rhoWNB->size[1] == 0) {
          b_y = 0.0;
        } else {
          b_y = rhoWNB->data[b_i];
          for (addB = 2; addB <= i; addB++) {
            b_y += x->data[addB - 1];
          }
        }

        rhoRouter->data[b_i] = b_y;
        addB = sigmaWNB->size[1];
        i = x->size[0] * x->size[1];
        x->size[0] = 1;
        x->size[1] = sigmaWNB->size[1];
        emxEnsureCapacity_real_T(x, i);
        for (i = 0; i < addB; i++) {
          x->data[i] = sigmaWNB->data[b_i + sigmaWNB->size[0] * i];
        }

        i = sigmaWNB->size[1];
        if (sigmaWNB->size[1] == 0) {
          b_y = 0.0;
        } else {
          b_y = sigmaWNB->data[b_i];
          for (addB = 2; addB <= i; addB++) {
            b_y += x->data[addB - 1];
          }
        }

        sigmaRouter->data[b_i] = b_y;
        for (p = 0; p < b_i; p++) {
          i = N - b_i;
          for (l = 0; l < i; l++) {
            idx = (int)((unsigned int)b_i + l);
            rhoRouter->data[b_i] = (rhoRouter->data[b_i] + rhoW->data[p +
              rhoW->size[0] * idx]) + rhoS->data[p + rhoS->size[0] * idx];
            sigmaRouter->data[b_i] = (sigmaRouter->data[b_i] + sigmaW->data[p +
              sigmaW->size[0] * idx]) + sigmaS->data[p + sigmaS->size[0] * idx];
          }

          for (l = 0; l < p; l++) {
            rhoRouter->data[b_i] = (rhoRouter->data[b_i] + rhoW->data[p +
              rhoW->size[0] * l]) + rhoS->data[p + rhoS->size[0] * l];
            sigmaRouter->data[b_i] = (sigmaRouter->data[b_i] + sigmaW->data[p +
              sigmaW->size[0] * l]) + sigmaS->data[p + sigmaS->size[0] * l];
          }
        }

        i = N - b_i;
        for (p = 0; p <= i - 2; p++) {
          b_p = ((unsigned int)b_i + p) + 2U;
          y = fmod((double)b_p - 1.0, 4.294967296E+9);
          if (y < 0.0) {
            l = -(int)(unsigned int)-y;
          } else {
            l = (int)(unsigned int)y;
          }

          i1 = l - b_i;
          for (l = 0; l < i1; l++) {
            idx = (int)b_p - 1;
            n = (int)((unsigned int)b_i + l);
            rhoRouter->data[b_i] = (rhoRouter->data[b_i] + rhoW->data[idx +
              rhoW->size[0] * n]) + rhoS->data[idx + rhoS->size[0] * n];
            sigmaRouter->data[b_i] = (sigmaRouter->data[b_i] + sigmaW->data[idx
              + sigmaW->size[0] * n]) + sigmaS->data[idx + sigmaS->size[0] * n];
          }
        }
      }

      /* compute maximals */
      n = rhoRouter->size[0];
      if (rhoRouter->size[0] <= 2) {
        if (rhoRouter->size[0] == 1) {
          rhoMax = rhoRouter->data[0];
        } else if ((rhoRouter->data[0] < rhoRouter->data[1]) || (rtIsNaN
                    (rhoRouter->data[0]) && (!rtIsNaN(rhoRouter->data[1])))) {
          rhoMax = rhoRouter->data[1];
        } else {
          rhoMax = rhoRouter->data[0];
        }
      } else {
        if (!rtIsNaN(rhoRouter->data[0])) {
          idx = 1;
        } else {
          idx = 0;
          addB = 2;
          exitg6 = false;
          while ((!exitg6) && (addB <= rhoRouter->size[0])) {
            if (!rtIsNaN(rhoRouter->data[addB - 1])) {
              idx = addB;
              exitg6 = true;
            } else {
              addB++;
            }
          }
        }

        if (idx == 0) {
          rhoMax = rhoRouter->data[0];
        } else {
          rhoMax = rhoRouter->data[idx - 1];
          i = idx + 1;
          for (addB = i; addB <= n; addB++) {
            y = rhoRouter->data[addB - 1];
            if (rhoMax < y) {
              rhoMax = y;
            }
          }
        }
      }

      n = sigmaRouter->size[0];
      if (sigmaRouter->size[0] <= 2) {
        if (sigmaRouter->size[0] == 1) {
          sigmaMax = sigmaRouter->data[0];
        } else if ((sigmaRouter->data[0] < sigmaRouter->data[1]) || (rtIsNaN
                    (sigmaRouter->data[0]) && (!rtIsNaN(sigmaRouter->data[1]))))
        {
          sigmaMax = sigmaRouter->data[1];
        } else {
          sigmaMax = sigmaRouter->data[0];
        }
      } else {
        if (!rtIsNaN(sigmaRouter->data[0])) {
          idx = 1;
        } else {
          idx = 0;
          addB = 2;
          exitg6 = false;
          while ((!exitg6) && (addB <= sigmaRouter->size[0])) {
            if (!rtIsNaN(sigmaRouter->data[addB - 1])) {
              idx = addB;
              exitg6 = true;
            } else {
              addB++;
            }
          }
        }

        if (idx == 0) {
          sigmaMax = sigmaRouter->data[0];
        } else {
          sigmaMax = sigmaRouter->data[idx - 1];
          i = idx + 1;
          for (addB = i; addB <= n; addB++) {
            y = sigmaRouter->data[addB - 1];
            if (sigmaMax < y) {
              sigmaMax = y;
            }
          }
        }
      }

      /* sum all, NB and B */
      /* can't have rate greater than one */
      if (!(rhoMax >= 1.0)) {
        /* compute buffer bound */
        sum(sigmaW, x);
        idx = x->size[1];
        if (x->size[1] == 0) {
          b_y = 0.0;
        } else {
          b_y = x->data[0];
          for (addB = 2; addB <= idx; addB++) {
            b_y += x->data[addB - 1];
          }
        }

        sum(sigmaS, x);
        idx = x->size[1];
        if (x->size[1] == 0) {
          y = 0.0;
        } else {
          y = x->data[0];
          for (addB = 2; addB <= idx; addB++) {
            y += x->data[addB - 1];
          }
        }

        sigmaMax = floor((double)N * (double)N * rhoMax / (1.0 - rhoMax) *
                         sigmaMax + (b_y + y));
        i = B->size[0];
        B->size[0] = backpressure->size[0];
        emxEnsureCapacity_real_T(B, i);
        addB = backpressure->size[0];
        for (i = 0; i < addB; i++) {
          B->data[i] = (1.0 - backpressure->data[i]) * sigmaMax;
        }

        /* determine Latency for west flows NB */
        /* delay is Bbound */
        /* for latency add hop distance */
        i = FlightLatencyW->size[0] * FlightLatencyW->size[1];
        FlightLatencyW->size[0] = N;
        FlightLatencyW->size[1] = N;
        emxEnsureCapacity_real_T(FlightLatencyW, i);
        for (i = 0; i < loop_ub_tmp; i++) {
          FlightLatencyW->data[i] = 0.0;
        }

        for (b_i = 0; b_i < N; b_i++) {
          for (n = 0; n < N; n++) {
            if (sigmaWNB->data[b_i + sigmaWNB->size[0] * n] == 0.0) {
              FlightLatencyW->data[b_i + FlightLatencyW->size[0] * n] = 0.0;
            } else {
              if (n + 1 >= b_i + 1) {
                y = (double)(n - b_i) + 1.0;
              } else {
                y = ((double)(n - b_i) + 1.0) + (double)N;
              }

              FlightLatencyW->data[b_i + FlightLatencyW->size[0] * n] = sigmaMax
                + y;
            }
          }
        }

        /* floor the latencies  */
        /* backpressuring flows. These are the N->S on each backpressured router */
        /* for the NB coming either N->S, the sigma is simply the buffer size Bbound */
        i = rhoBack->size[0];
        rhoBack->size[0] = N;
        emxEnsureCapacity_real_T(rhoBack, i);
        for (i = 0; i < N; i++) {
          rhoBack->data[i] = 0.0;
        }

        i = burstBack->size[0];
        burstBack->size[0] = N;
        emxEnsureCapacity_real_T(burstBack, i);
        for (b_i = 0; b_i < N; b_i++) {
          burstBack->data[b_i] = 0.0;
          if (!(backpressure->data[b_i] == 0.0)) {
            rhoMax = 0.0;
            for (p = 0; p < b_i; p++) {
              addB = 0;
              i = N - b_i;
              if (0 <= i - 1) {
                rhoBack->data[b_i] = 1.0 - R->data[b_i];
              }

              for (l = 0; l < i; l++) {
                idx = (int)((unsigned int)b_i + l);
                rhoMax = (rhoMax + sigmaWB->data[p + sigmaWB->size[0] * idx]) +
                  sigmaS->data[p + sigmaS->size[0] * idx];
                if (sigmaWNB->data[p + sigmaWNB->size[0] * idx] > 0.0) {
                  addB = 1;
                }
              }

              if (0 <= p - 1) {
                rhoBack->data[b_i] = 1.0 - R->data[b_i];
              }

              for (l = 0; l < p; l++) {
                rhoMax = (rhoMax + sigmaWB->data[p + sigmaWB->size[0] * l]) +
                  sigmaS->data[p + sigmaS->size[0] * l];
                if (sigmaWNB->data[p + sigmaWNB->size[0] * l] > 0.0) {
                  addB = 1;
                }
              }

              rhoMax += (double)addB * sigmaMax;
            }

            i = N - b_i;
            for (p = 0; p <= i - 2; p++) {
              b_p = ((unsigned int)b_i + p) + 2U;
              addB = 0;
              y = fmod((double)b_p - 1.0, 4.294967296E+9);
              if (y < 0.0) {
                l = -(int)(unsigned int)-y;
              } else {
                l = (int)(unsigned int)y;
              }

              i1 = l - b_i;
              if (0 <= i1 - 1) {
                rhoBack->data[b_i] = 1.0 - R->data[b_i];
              }

              for (l = 0; l < i1; l++) {
                idx = (int)b_p - 1;
                n = (int)((unsigned int)b_i + l);
                rhoMax = (rhoMax + sigmaWB->data[idx + sigmaWB->size[0] * n]) +
                  sigmaS->data[idx + sigmaS->size[0] * n];
                if (sigmaWNB->data[idx + sigmaWNB->size[0] * n] > 0.0) {
                  addB = 1;
                }
              }

              rhoMax += (double)addB * sigmaMax;
            }

            if ((rhoMax > 0.0) || (rhoBack->data[b_i] > 0.0)) {
              burstBack->data[b_i] = ceil((rhoMax + 1.0) + rhoBack->data[b_i]);
            }
          }
        }

        /* compute the interfering sigma from N->S and W->S (both B and NB) on S port of each node i */
        /* for the NB coming either N->S or W->S, the sigma is simply the buffer size Bbound */
        i = rhoRouter->size[0];
        rhoRouter->size[0] = N;
        emxEnsureCapacity_real_T(rhoRouter, i);
        for (i = 0; i < N; i++) {
          rhoRouter->data[i] = 0.0;
        }

        for (b_i = 0; b_i < N; b_i++) {
          /* W->S */
          addB = sigmaWB->size[1];
          i = x->size[0] * x->size[1];
          x->size[0] = 1;
          x->size[1] = sigmaWB->size[1];
          emxEnsureCapacity_real_T(x, i);
          for (i = 0; i < addB; i++) {
            x->data[i] = sigmaWB->data[b_i + sigmaWB->size[0] * i];
          }

          i = sigmaWB->size[1];
          if (sigmaWB->size[1] == 0) {
            b_y = 0.0;
          } else {
            b_y = sigmaWB->data[b_i];
            for (addB = 2; addB <= i; addB++) {
              b_y += x->data[addB - 1];
            }
          }

          rhoRouter->data[b_i] = b_y;
          addB = sigmaWNB->size[1];
          i = b_sigmaWNB->size[0] * b_sigmaWNB->size[1];
          b_sigmaWNB->size[0] = 1;
          b_sigmaWNB->size[1] = sigmaWNB->size[1];
          emxEnsureCapacity_boolean_T(b_sigmaWNB, i);
          for (i = 0; i < addB; i++) {
            b_sigmaWNB->data[i] = (sigmaWNB->data[b_i + sigmaWNB->size[0] * i] >
              0.0);
          }

          if (b_any(b_sigmaWNB)) {
            rhoRouter->data[b_i] += sigmaMax;
          }

          /* N->S */
          for (p = 0; p < b_i; p++) {
            addB = 0;
            i = N - b_i;
            for (l = 0; l < i; l++) {
              idx = (int)((unsigned int)b_i + l);
              rhoRouter->data[b_i] = (rhoRouter->data[b_i] + sigmaWB->data[p +
                sigmaWB->size[0] * idx]) + sigmaS->data[p + sigmaS->size[0] *
                idx];
              if (sigmaWNB->data[p + sigmaWNB->size[0] * idx] > 0.0) {
                addB = 1;
              }
            }

            for (l = 0; l < p; l++) {
              rhoRouter->data[b_i] = (rhoRouter->data[b_i] + sigmaWB->data[p +
                sigmaWB->size[0] * l]) + sigmaS->data[p + sigmaS->size[0] * l];
              if (sigmaWNB->data[p + sigmaWNB->size[0] * l] > 0.0) {
                addB = 1;
              }
            }

            rhoRouter->data[b_i] += (double)addB * sigmaMax;
          }

          i = N - b_i;
          for (p = 0; p <= i - 2; p++) {
            b_p = ((unsigned int)b_i + p) + 2U;
            addB = 0;
            y = fmod((double)b_p - 1.0, 4.294967296E+9);
            if (y < 0.0) {
              l = -(int)(unsigned int)-y;
            } else {
              l = (int)(unsigned int)y;
            }

            i1 = l - b_i;
            for (l = 0; l < i1; l++) {
              idx = (int)b_p - 1;
              n = (int)((unsigned int)b_i + l);
              rhoRouter->data[b_i] = (rhoRouter->data[b_i] + sigmaWB->data[idx +
                sigmaWB->size[0] * n]) + sigmaS->data[idx + sigmaS->size[0] * n];
              if (sigmaWNB->data[idx + sigmaWNB->size[0] * n] > 0.0) {
                addB = 1;
              }
            }

            rhoRouter->data[b_i] += (double)addB * sigmaMax;
          }
        }

        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        /* BUFFER BOUND ENDS */
        /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
        guard1 = true;
      }
    }

    if (guard1) {
      /* compute the interfering rate from N->S and W->S flows (B and NB) on S port of node i */
      /* R is already all north, so just add W->S */
      i = interrho->size[0];
      interrho->size[0] = N;
      emxEnsureCapacity_real_T(interrho, i);
      for (b_i = 0; b_i < N; b_i++) {
        addB = rhoW->size[1];
        i = x->size[0] * x->size[1];
        x->size[0] = 1;
        x->size[1] = rhoW->size[1];
        emxEnsureCapacity_real_T(x, i);
        for (i = 0; i < addB; i++) {
          x->data[i] = rhoW->data[b_i + rhoW->size[0] * i];
        }

        i = rhoW->size[1];
        if (rhoW->size[1] == 0) {
          b_y = 0.0;
        } else {
          b_y = rhoW->data[b_i];
          for (addB = 2; addB <= i; addB++) {
            b_y += x->data[addB - 1];
          }
        }

        interrho->data[b_i] = R->data[b_i] - b_y;
      }

      /* Latency for PE -> S */
      /* apply Theorem 2 */
      i = TotalLatencyS->size[0] * TotalLatencyS->size[1];
      TotalLatencyS->size[0] = N;
      TotalLatencyS->size[1] = N;
      emxEnsureCapacity_real_T(TotalLatencyS, i);
      for (i = 0; i < loop_ub_tmp; i++) {
        TotalLatencyS->data[i] = 0.0;
      }

      b_i = 0;
      do {
        exitg2 = 0;
        if (b_i <= N - 1) {
          n = 0;
          do {
            exitg1 = 0;
            if (n <= N - 1) {
              if (rhoS->data[b_i + rhoS->size[0] * n] == 0.0) {
                TotalLatencyS->data[b_i + TotalLatencyS->size[0] * n] = 0.0;
                n++;
              } else {
                /* conflicting b. Add to intersigma the PE->E, and all other PE->S */
                addB = burstS->size[1];
                i = x->size[0] * x->size[1];
                x->size[0] = 1;
                x->size[1] = burstS->size[1];
                emxEnsureCapacity_real_T(x, i);
                for (i = 0; i < addB; i++) {
                  x->data[i] = burstS->data[b_i + burstS->size[0] * i];
                }

                i = burstS->size[1];
                if (burstS->size[1] == 0) {
                  b_y = 0.0;
                } else {
                  b_y = burstS->data[b_i];
                  for (addB = 2; addB <= i; addB++) {
                    b_y += x->data[addB - 1];
                  }
                }

                rhoMax = (burstE->data[b_i] + b_y) - burstS->data[b_i +
                  burstS->size[0] * n];
                if ((rhoRouter->data[b_i] > 0.0) || (interrho->data[b_i] < 1.0))
                {
                  rhoMax += ceil(((rhoRouter->data[b_i] + 1.0) + 1.0) -
                                 interrho->data[b_i]);
                }

                /* conflicting rho. Add to interrho the PE->E, and all other PE->S */
                addB = rhoS->size[1];
                i = x->size[0] * x->size[1];
                x->size[0] = 1;
                x->size[1] = rhoS->size[1];
                emxEnsureCapacity_real_T(x, i);
                for (i = 0; i < addB; i++) {
                  x->data[i] = rhoS->data[b_i + rhoS->size[0] * i];
                }

                i = rhoS->size[1];
                if (rhoS->size[1] == 0) {
                  b_y = 0.0;
                } else {
                  b_y = rhoS->data[b_i];
                  for (addB = 2; addB <= i; addB++) {
                    b_y += x->data[addB - 1];
                  }
                }

                sigmaMax = ((interrho->data[b_i] - rhoE->data[b_i]) - b_y) +
                  rhoS->data[b_i + rhoS->size[0] * n];
                if (sigmaMax <= 0.0) {
                  exitg1 = 1;
                } else {
                  /* Note: removed this check - even if remaining rate is less than */
                  /* injection rate, latency is still correct */
                  /* if(rhoS(i,j) > confrho) */
                  /*    'remaining rate less than injection rate - cannot guarantee injection' */
                  /*   return */
                  /* end        */
                  y = rhoS->data[b_i + rhoS->size[0] * n];

                  /* COMPUTE_INJECTION_LATENCY Computes injection latency for a flow */
                  /*  */
                  /* k, rho are the original burstiness and the rate of the flow under analysis */
                  /* confb, confrho are the burstiness and remaining rate of all conflicting flows */
                  /*  */
                  /* for a flow injected to the E port, the conflicting flows are: */
                  /* 1. any other flow injected by the same PE (to the E or to the S) */
                  /* 2. any other flow crossing the switch W->E */
                  /*  */
                  /* for a flow injected to the S port, the conflicting flows are: */
                  /* 1. any other flow injected by the same PE (to the E or to the S) */
                  /* 2. any other flow coming from N->S */
                  /* 4. any other flow turning W->S */
                  /*  */
                  /* firstPacketOnly: 1 to compute injection latency for first packet only; */
                  /*                  0 to compute injection latency for all packets in the burst */
                  rhoMax = (ceil(rhoMax / sigmaMax) - 1.0) + ceil(1.0 / y);
                  if (!(firstPacketOnly == 1.0)) {
                    rhoMax += ceil((kS->data[b_i + kS->size[0] * n] - 1.0) *
                                   fmax(1.0 / y, 1.0 / sigmaMax));
                  }

                  if (n + 1 >= b_i + 1) {
                    y = (double)(n - b_i) + 1.0;
                  } else {
                    y = ((double)(n - b_i) + 1.0) + (double)N;
                  }

                  TotalLatencyS->data[b_i + TotalLatencyS->size[0] * n] = rhoMax
                    + y;
                  n++;
                }
              }
            } else {
              b_i++;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = 1;
          }
        } else {
          *retval = true;
          exitg2 = 1;
        }
      } while (exitg2 == 0);
    }
  }

  emxFree_boolean_T(&b_sigmaWNB);
  emxFree_boolean_T(&b_sigma_l);
  emxFree_real_T(&r1);
  emxFree_real_T(&x);
  emxFree_int8_T(&r);
  emxFree_real_T(&interrho);
  emxFree_real_T(&sigmaRouter);
  emxFree_real_T(&rhoRouter);
  emxFree_real_T(&rho_CT);
  emxFree_real_T(&CT);
  emxFree_real_T(&sigma_l);
  emxFree_real_T(&Rij);
  emxFree_real_T(&R);
  emxFree_real_T(&rhoWNB);
  emxFree_real_T(&rhoWB);
  emxFree_real_T(&sigmaWNB);
  emxFree_real_T(&sigmaWB);
  emxFree_real_T(&sigmaS);
  emxFree_real_T(&sigmaW);
}

/*
 * File trailer for west_fifo_extra_backpressure.c
 *
 * [EOF]
 */
