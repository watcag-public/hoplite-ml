/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_extra_backpressure.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef WEST_FIFO_EXTRA_BACKPRESSURE_H
#define WEST_FIFO_EXTRA_BACKPRESSURE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern void west_fifo_extra_backpressure(const emxArray_real_T *burstW, const
  emxArray_real_T *rhoW, const emxArray_real_T *burstS, const emxArray_real_T
  *kS, const emxArray_real_T *rhoS, const emxArray_real_T *burstE, const
  emxArray_real_T *rhoE, const emxArray_real_T *backpressure, double method,
  double firstPacketOnly, boolean_T *retval, emxArray_real_T *B, emxArray_real_T
  *FlightLatencyW, emxArray_real_T *TotalLatencyS, emxArray_real_T *burstBack,
  emxArray_real_T *rhoBack);

#endif

/*
 * File trailer for west_fifo_extra_backpressure.h
 *
 * [EOF]
 */
