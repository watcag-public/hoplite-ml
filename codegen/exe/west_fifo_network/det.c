/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: det.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "det.h"
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include "west_fifo_network_emxutil.h"
#include "xzgetrf.h"

/* Function Definitions */

/*
 * Arguments    : const emxArray_real_T *x
 * Return Type  : double
 */
double det(const emxArray_real_T *x)
{
  double y;
  emxArray_real_T *b_x;
  int i;
  int loop_ub;
  emxArray_int32_T *ipiv;
  boolean_T isodd;
  if ((x->size[0] == 0) || (x->size[1] == 0)) {
    y = 1.0;
  } else {
    emxInit_real_T(&b_x, 2);
    i = b_x->size[0] * b_x->size[1];
    b_x->size[0] = x->size[0];
    b_x->size[1] = x->size[1];
    emxEnsureCapacity_real_T(b_x, i);
    loop_ub = x->size[0] * x->size[1];
    for (i = 0; i < loop_ub; i++) {
      b_x->data[i] = x->data[i];
    }

    emxInit_int32_T(&ipiv, 2);
    xzgetrf(x->size[0], x->size[1], b_x, x->size[0], ipiv, &loop_ub);
    y = b_x->data[0];
    i = b_x->size[0];
    for (loop_ub = 0; loop_ub <= i - 2; loop_ub++) {
      y *= b_x->data[(loop_ub + b_x->size[0] * (loop_ub + 1)) + 1];
    }

    emxFree_real_T(&b_x);
    isodd = false;
    i = ipiv->size[1];
    for (loop_ub = 0; loop_ub <= i - 2; loop_ub++) {
      if (ipiv->data[loop_ub] > loop_ub + 1) {
        isodd = !isodd;
      }
    }

    emxFree_int32_T(&ipiv);
    if (isodd) {
      y = -y;
    }
  }

  return y;
}

/*
 * File trailer for det.c
 *
 * [EOF]
 */
