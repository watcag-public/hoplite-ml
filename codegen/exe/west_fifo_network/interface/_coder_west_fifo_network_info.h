/* 
 * Academic License - for use in teaching, academic research, and meeting 
 * course requirements at degree granting institutions only.  Not for 
 * government, commercial, or other organizational use. 
 * File: _coder_west_fifo_network_info.h 
 *  
 * MATLAB Coder version            : 4.2 
 * C/C++ source code generated on  : 05-Sep-2019 18:44:43 
 */

#ifndef _CODER_WEST_FIFO_NETWORK_INFO_H
#define _CODER_WEST_FIFO_NETWORK_INFO_H
/* Include Files */ 
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */ 
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif
/* 
 * File trailer for _coder_west_fifo_network_info.h 
 *  
 * [EOF] 
 */
