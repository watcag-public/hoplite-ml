/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_west_fifo_network_api.h
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 05-Sep-2019 18:44:43
 */

#ifndef _CODER_WEST_FIFO_NETWORK_API_H
#define _CODER_WEST_FIFO_NETWORK_API_H

/* Include Files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_west_fifo_network_api.h"

/* Type Definitions */
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void MEXGlobalSyncInFunction(const emlrtStack *sp);
extern void MEXGlobalSyncOutFunction(boolean_T skipDirtyCheck);
extern void west_fifo_network(real_T columns, real_T rows, emxArray_real_T
  *burst, emxArray_real_T *rho, emxArray_real_T *backpressure, real_T method,
  real_T firstPacketOnly, real_T *retval, emxArray_real_T *B, emxArray_real_T
  *Latency);
extern void west_fifo_network_api(const mxArray * const prhs[7], int32_T nlhs,
  const mxArray *plhs[3]);
extern void west_fifo_network_atexit(void);
extern void west_fifo_network_initialize(void);
extern void west_fifo_network_terminate(void);
extern void west_fifo_network_xil_shutdown(void);
extern void west_fifo_network_xil_terminate(void);

#endif

/*
 * File trailer for _coder_west_fifo_network_api.h
 *
 * [EOF]
 */
