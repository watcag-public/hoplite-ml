/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_west_fifo_network_mex.h
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 05-Sep-2019 18:44:43
 */

#ifndef _CODER_WEST_FIFO_NETWORK_MEX_H
#define _CODER_WEST_FIFO_NETWORK_MEX_H

/* Include Files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_west_fifo_network_api.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);
extern emlrtCTX mexFunctionCreateRootTLS(void);

#endif

/*
 * File trailer for _coder_west_fifo_network_mex.h
 *
 * [EOF]
 */
