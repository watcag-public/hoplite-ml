/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_network.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef WEST_FIFO_NETWORK_H
#define WEST_FIFO_NETWORK_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern void west_fifo_network(double columns, double rows, emxArray_real_T
  *burst, const emxArray_real_T *rho, const emxArray_real_T *backpressure,
  double method, double firstPacketOnly, double *retval, emxArray_real_T *B,
  emxArray_real_T *Latency);

#endif

/*
 * File trailer for west_fifo_network.h
 *
 * [EOF]
 */
