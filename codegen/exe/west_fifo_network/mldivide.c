/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mldivide.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "mldivide.h"
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include "west_fifo_network_emxutil.h"
#include "xzgeqp3.h"
#include "xzgetrf.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const emxArray_real_T *A
 *                emxArray_real_T *B
 * Return Type  : void
 */
void mldivide(const emxArray_real_T *A, emxArray_real_T *B)
{
  emxArray_real_T *b_A;
  emxArray_real_T *tau;
  emxArray_int32_T *jpvt;
  emxArray_real_T *b_B;
  int i;
  int mn;
  int minmana;
  int maxmn;
  double tol;
  int b_i;
  int rankR;
  emxInit_real_T(&b_A, 2);
  emxInit_real_T(&tau, 1);
  emxInit_int32_T(&jpvt, 2);
  emxInit_real_T(&b_B, 1);
  if ((A->size[0] == 0) || (A->size[1] == 0) || (B->size[0] == 0)) {
    i = B->size[0];
    B->size[0] = A->size[1];
    emxEnsureCapacity_real_T(B, i);
    minmana = A->size[1];
    for (i = 0; i < minmana; i++) {
      B->data[i] = 0.0;
    }
  } else if (A->size[0] == A->size[1]) {
    mn = A->size[1];
    i = b_A->size[0] * b_A->size[1];
    b_A->size[0] = A->size[0];
    b_A->size[1] = A->size[1];
    emxEnsureCapacity_real_T(b_A, i);
    minmana = A->size[0] * A->size[1];
    for (i = 0; i < minmana; i++) {
      b_A->data[i] = A->data[i];
    }

    xzgetrf(A->size[1], A->size[1], b_A, A->size[1], jpvt, &minmana);
    i = A->size[1];
    for (maxmn = 0; maxmn <= i - 2; maxmn++) {
      minmana = jpvt->data[maxmn];
      if (minmana != maxmn + 1) {
        tol = B->data[maxmn];
        B->data[maxmn] = B->data[minmana - 1];
        B->data[minmana - 1] = tol;
      }
    }

    for (maxmn = 0; maxmn < mn; maxmn++) {
      minmana = mn * maxmn;
      if (B->data[maxmn] != 0.0) {
        i = maxmn + 2;
        for (b_i = i; b_i <= mn; b_i++) {
          B->data[b_i - 1] -= B->data[maxmn] * b_A->data[(b_i + minmana) - 1];
        }
      }
    }

    for (maxmn = mn; maxmn >= 1; maxmn--) {
      minmana = mn * (maxmn - 1);
      tol = B->data[maxmn - 1];
      if (tol != 0.0) {
        B->data[maxmn - 1] = tol / b_A->data[(maxmn + minmana) - 1];
        for (b_i = 0; b_i <= maxmn - 2; b_i++) {
          B->data[b_i] -= B->data[maxmn - 1] * b_A->data[b_i + minmana];
        }
      }
    }
  } else {
    i = b_A->size[0] * b_A->size[1];
    b_A->size[0] = A->size[0];
    b_A->size[1] = A->size[1];
    emxEnsureCapacity_real_T(b_A, i);
    minmana = A->size[0] * A->size[1];
    for (i = 0; i < minmana; i++) {
      b_A->data[i] = A->data[i];
    }

    mn = A->size[1];
    maxmn = A->size[0];
    minmana = A->size[1];
    if (maxmn < minmana) {
      minmana = maxmn;
    }

    i = tau->size[0];
    tau->size[0] = minmana;
    emxEnsureCapacity_real_T(tau, i);
    for (i = 0; i < minmana; i++) {
      tau->data[i] = 0.0;
    }

    i = jpvt->size[0] * jpvt->size[1];
    jpvt->size[0] = 1;
    jpvt->size[1] = A->size[1];
    emxEnsureCapacity_int32_T(jpvt, i);
    minmana = A->size[1];
    for (i = 0; i < minmana; i++) {
      jpvt->data[i] = 0;
    }

    for (maxmn = 0; maxmn < mn; maxmn++) {
      jpvt->data[maxmn] = maxmn + 1;
    }

    qrpf(b_A, A->size[0], A->size[1], tau, jpvt);
    rankR = 0;
    if (b_A->size[0] < b_A->size[1]) {
      minmana = b_A->size[0];
      maxmn = b_A->size[1];
    } else {
      minmana = b_A->size[1];
      maxmn = b_A->size[0];
    }

    if (minmana > 0) {
      tol = fmin(1.4901161193847656E-8, 2.2204460492503131E-15 * (double)maxmn) *
        fabs(b_A->data[0]);
      while ((rankR < minmana) && (!(fabs(b_A->data[rankR + b_A->size[0] * rankR])
               <= tol))) {
        rankR++;
      }
    }

    i = b_B->size[0];
    b_B->size[0] = B->size[0];
    emxEnsureCapacity_real_T(b_B, i);
    minmana = B->size[0];
    for (i = 0; i < minmana; i++) {
      b_B->data[i] = B->data[i];
    }

    i = B->size[0];
    B->size[0] = b_A->size[1];
    emxEnsureCapacity_real_T(B, i);
    minmana = b_A->size[1];
    for (i = 0; i < minmana; i++) {
      B->data[i] = 0.0;
    }

    minmana = b_A->size[0];
    maxmn = b_A->size[0];
    mn = b_A->size[1];
    if (maxmn < mn) {
      mn = maxmn;
    }

    for (maxmn = 0; maxmn < mn; maxmn++) {
      if (tau->data[maxmn] != 0.0) {
        tol = b_B->data[maxmn];
        i = maxmn + 2;
        for (b_i = i; b_i <= minmana; b_i++) {
          tol += b_A->data[(b_i + b_A->size[0] * maxmn) - 1] * b_B->data[b_i - 1];
        }

        tol *= tau->data[maxmn];
        if (tol != 0.0) {
          b_B->data[maxmn] -= tol;
          i = maxmn + 2;
          for (b_i = i; b_i <= minmana; b_i++) {
            b_B->data[b_i - 1] -= b_A->data[(b_i + b_A->size[0] * maxmn) - 1] *
              tol;
          }
        }
      }
    }

    for (b_i = 0; b_i < rankR; b_i++) {
      B->data[jpvt->data[b_i] - 1] = b_B->data[b_i];
    }

    for (maxmn = rankR; maxmn >= 1; maxmn--) {
      i = jpvt->data[maxmn - 1];
      B->data[i - 1] /= b_A->data[(maxmn + b_A->size[0] * (maxmn - 1)) - 1];
      for (b_i = 0; b_i <= maxmn - 2; b_i++) {
        B->data[jpvt->data[b_i] - 1] -= B->data[jpvt->data[maxmn - 1] - 1] *
          b_A->data[b_i + b_A->size[0] * (maxmn - 1)];
      }
    }
  }

  emxFree_real_T(&b_B);
  emxFree_int32_T(&jpvt);
  emxFree_real_T(&tau);
  emxFree_real_T(&b_A);
}

/*
 * File trailer for mldivide.c
 *
 * [EOF]
 */
