/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xzgeqp3.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef XZGEQP3_H
#define XZGEQP3_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern void qrpf(emxArray_real_T *A, int m, int n, emxArray_real_T *tau,
                 emxArray_int32_T *jpvt);

#endif

/*
 * File trailer for xzgeqp3.h
 *
 * [EOF]
 */
