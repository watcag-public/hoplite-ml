/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xzgetrf.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef XZGETRF_H
#define XZGETRF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern void xzgetrf(int m, int n, emxArray_real_T *A, int lda, emxArray_int32_T *
                    ipiv, int *info);

#endif

/*
 * File trailer for xzgetrf.h
 *
 * [EOF]
 */
