//#define DEBUG
/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: main.c
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 31-Aug-2019 15:27:51
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include Files */
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include "test_C.h"
#include "west_fifo_network_terminate.h"
#include "west_fifo_network_emxAPI.h"
#include "west_fifo_network_initialize.h"
#include <stdio.h>
#include <time.h>
#include <string.h>

/* Function Declarations */
static emxArray_real_T *argInit_Unboundedx1_real_T(int, double*);
static double argInit_real_T(void);
static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(int, double*);

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : emxArray_real_T *
 */
static emxArray_real_T *argInit_Unboundedx1_real_T(int N, double* data)
{
  emxArray_real_T *result;
  static int iv0[1];
  iv0[0] = N*N;

  int idx0;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreateND_real_T(1, iv0);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result->data[idx0] = data[idx0];
  }

  return result;
}

/*
 * Arguments    : void
 * Return Type  : double
 */
static double argInit_real_T(void)
{
  return 0.0;
}

/*
 * Arguments    : void
 * Return Type  : emxArray_real_T *
 */
static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(int N, double* data)
{
  emxArray_real_T *result;
  int idx0;
  int idx1;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreate_real_T(N, N);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < result->size[1U]; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result->data[idx0 + result->size[0] * idx1] = data[idx0 + result->size[0] * idx1];
    }
  }

  return result;
}

static double *burst_data;
static double *rho_data;
static double *backpressure_data;

static double retval;
static int first_time=1;
static double *wclatency_data;
static double *buffer_data;

void main_west_fifo_network_initialize(int N) {
  west_fifo_network_initialize();
  
  burst_data = malloc(sizeof(double)*(N*N*N*N));
  rho_data = malloc(sizeof(double)*(N*N*N*N));
  backpressure_data = malloc(sizeof(double)*(N*N));
  
  char buffer[4096] ;
  char *record,*line;
  int i=0,j=0;
  FILE *fstream;

  // read csv file to 2d array as in https://stackoverflow.com/questions/20013693/read-csv-file-to-a-2d-array-on-c/20014008
  fstream = fopen("pe_bm_0.csv","r");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
  {
    record = strtok(line,",");
    j=0;
    while(record != NULL)
    {
      burst_data[i+N*N*j] = atof(record) ;
#ifdef DEBUG
      printf("burst_data[%d][%d]:%f\n",i,j,burst_data[j+N*N*i]) ;    //here you can put the record into the array as per your requirement.
#endif
      j++;
      record = strtok(NULL,",");
    }
    ++i ;
  }
  fclose(fstream);

  i=0;j=0;
  fstream = fopen("pe_rm_0.csv","r");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
  {
    record = strtok(line,",");
    j=0;
    while(record != NULL)
    {
      rho_data[i+N*N*j] = atof(record) ;
#ifdef DEBUG
      printf("rho_data[%d][%d]:%f\n",i,j,rho_data[j+N*N*i]) ;    //here you can put the record into the array as per your requirement.
#endif
      j++;
      record = strtok(NULL,",");
    }
    ++i ;
  }
  fclose(fstream);

  i=0;j=0;
  fstream = fopen("sw_bp_0.csv","r");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
  {
#ifdef DEBUG
    printf("line: %s",line) ;    //here you can put the record into the array as per your requirement.
#endif
    backpressure_data[j++] = atof(line) ;
  }
  fclose(fstream);

}

void main_west_fifo_network(int N) {

  main_west_fifo_network_initialize(N);
  main_west_fifo_network_with_args(N, burst_data, rho_data, backpressure_data);
  main_west_fifo_network_terminate(N);

}

/*
 * Arguments    : void
 * Return Type  : void
 */
int main_west_fifo_network_with_args(int N, double* burst_data, double* rho_data, double* backpressure_data) {

  emxArray_real_T *B;
  emxArray_real_T *Latency;
  emxArray_real_T *burst;
  emxArray_real_T *rho;
  emxArray_real_T *backpressure;
  emxInitArray_real_T(&B, 1);
  emxInitArray_real_T(&Latency, 2);

  /* Initialize function 'west_fifo_network' input arguments. */
#ifdef DEBUG
  printf("burst_data=\n");
  for(int i=0;i<N*N;i++) {
    for(int j=0;j<N*N;j++) {
      printf("%d,", (int)burst_data[i+N*N*j]);
    }
    printf("\n");
  }
  printf("rho_data=\n");
  for(int i=0;i<N*N;i++) {
    for(int j=0;j<N*N;j++) {
      printf("%f,", rho_data[i+N*N*j]);
    }
    printf("\n");
  }
  printf("backpressure_data=\n");
  for(int j=0;j<N*N;j++) {
    printf("%f,", backpressure_data[j]);
  }
  printf("\n");
#endif

  /* Initialize function input argument 'burst'. */
  burst = c_argInit_UnboundedxUnbounded_r(N*N,burst_data);

  /* Initialize function input argument 'rho'. */
  rho = c_argInit_UnboundedxUnbounded_r(N*N,rho_data);

  /* Initialize function input argument 'backpressure'. */
  backpressure = argInit_Unboundedx1_real_T(N,backpressure_data);

  /* Call the entry-point 'west_fifo_network'. */
  retval=0;
  west_fifo_network(N, N, burst, rho, backpressure, 0, 0, &retval, B, Latency);

  if(first_time) {
    first_time=0;
    wclatency_data = malloc(sizeof(double)*(N*N*N*N));
    buffer_data = malloc(sizeof(double)*(N*N));
  }

  /* Save data for later retrieval */
  for(int j=0;j<N*N;j++) {
      buffer_data[j] = B->data[j];
  }

  for(int i=0;i<N*N;i++) {
    for(int j=0;j<N*N;j++) {
      wclatency_data[i+N*N*j] = Latency->data[i+N*N*j];
    }
  }

#ifdef DEBUG
  printf("Buffer_data =\n");
  for(int j=0;j<N*N;j++) {
      printf("%d,", (int)B->data[j]);
  }

  printf("Latency_data=\n");
  for(int i=0;i<N*N;i++) {
    for(int j=0;j<N*N;j++) {
      printf("%d,", (int)Latency->data[i+N*N*j]);
    }
    printf("\n");
  }
#endif

  emxDestroyArray_real_T(Latency);
  emxDestroyArray_real_T(B);
  emxDestroyArray_real_T(backpressure);
  emxDestroyArray_real_T(rho);
  emxDestroyArray_real_T(burst);

  return (int)retval;
}

// in case you want a CSV pathway
void main_west_fifo_network_terminate(int N) {
  FILE* fstream = fopen("buffer_0.csv","w");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  for(int j=0;j<N*N;j++)
  {
    fprintf(fstream, "%d\n", (int)burst_data[j]);
  }
  fclose(fstream);

  fstream = fopen("total_0.csv","w");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  for(int i=0;i<N*N;i++) {
    fprintf(fstream, "%d", (int)wclatency_data[i]);
    for(int j=1;j<N*N;j++) {
      fprintf(fstream, ",%d", (int)wclatency_data[i+N*N*j]);
    }
    fprintf(fstream,"\n");
  }
  fclose(fstream);

  fstream = fopen("stable_0.csv","w");
  if(fstream == NULL) {
    printf("\n file opening failed ");
    exit(1);
  }
  fprintf(fstream, "%d\n", (int)retval);
  fclose(fstream);

  west_fifo_network_terminate();
}

double* get_buffer() {
  return buffer_data;
}

double* get_wclatency() {
  return wclatency_data;
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
  /* Initialize the application.
     You do not need to do this more than one time. */
  west_fifo_network_initialize();
	
  //printf("init done\n");

  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_west_fifo_network(atoi(argv[1]));

  //printf("main done\n");

  /* Terminate the application.
     You do not need to do this more than one time. */
  west_fifo_network_terminate();
  return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
