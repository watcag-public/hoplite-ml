/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mod.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "mod.h"
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : double x
 *                double y
 * Return Type  : double
 */
double b_mod(double x, double y)
{
  double r;
  r = x;
  if (!(y == 0.0)) {
    if (rtIsNaN(x) || rtIsNaN(y) || rtIsInf(x)) {
      r = rtNaN;
    } else {
      if (!rtIsInf(y)) {
        r = fmod(x, y);
        if (r == 0.0) {
          r = y * 0.0;
        }
      }
    }
  }

  return r;
}

/*
 * File trailer for mod.c
 *
 * [EOF]
 */
