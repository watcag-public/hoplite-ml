/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_network_terminate.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

/* Include Files */
#include "west_fifo_network_terminate.h"
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include "west_fifo_network_data.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void west_fifo_network_terminate(void)
{
  /* (no terminate code required) */
  isInitialized_west_fifo_network = false;
}

/*
 * File trailer for west_fifo_network_terminate.c
 *
 * [EOF]
 */
