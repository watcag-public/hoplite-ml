/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: any.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef ANY_H
#define ANY_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern boolean_T any(const emxArray_boolean_T *x);
extern boolean_T b_any(const emxArray_boolean_T *x);

#endif

/*
 * File trailer for any.h
 *
 * [EOF]
 */
