/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: west_fifo_network_data.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef WEST_FIFO_NETWORK_DATA_H
#define WEST_FIFO_NETWORK_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Variable Declarations */
extern double colrc;
extern boolean_T isInitialized_west_fifo_network;

#endif

/*
 * File trailer for west_fifo_network_data.h
 *
 * [EOF]
 */
