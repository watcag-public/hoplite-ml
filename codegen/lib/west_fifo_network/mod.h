/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: mod.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 12-Jan-2020 19:56:58
 */

#ifndef MOD_H
#define MOD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "west_fifo_network_types.h"

/* Function Declarations */
extern double b_mod(double x, double y);

#endif

/*
 * File trailer for mod.h
 *
 * [EOF]
 */
