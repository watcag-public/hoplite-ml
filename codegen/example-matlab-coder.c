/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: main.c
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 31-Aug-2019 15:27:51
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include Files */
#include "rt_nonfinite.h"
#include "west_fifo_network.h"
#include "example-matlab-coder.h"
#include "west_fifo_network_terminate.h"
#include "west_fifo_network_emxAPI.h"
#include "west_fifo_network_initialize.h"
#include <stdio.h>
#include <time.h>

/* Function Declarations */
static emxArray_real_T *argInit_Unboundedx1_real_T(double*);
static double argInit_real_T(void);
static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(double*);
static void main_west_fifo_network(void);

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : emxArray_real_T *
 */
static emxArray_real_T *argInit_Unboundedx1_real_T(double* data)
{
  emxArray_real_T *result;
  static int iv0[1] = { 25 };

  int idx0;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreateND_real_T(1, iv0);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result->data[idx0] = data[idx0];
  }

  return result;
}

/*
 * Arguments    : void
 * Return Type  : double
 */
static double argInit_real_T(void)
{
  return 0.0;
}

/*
 * Arguments    : void
 * Return Type  : emxArray_real_T *
 */
static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(double* data)
{
  emxArray_real_T *result;
  int idx0;
  int idx1;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreate_real_T(25, 25);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < result->size[1U]; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result->data[idx0 + result->size[0] * idx1] = data[idx0 + result->size[0] * idx1];
    }
  }

  return result;
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_west_fifo_network(void)
{
  emxArray_real_T *B;
  emxArray_real_T *Latency;
  emxArray_real_T *burst;
  emxArray_real_T *rho;
  emxArray_real_T *backpressure;
  double retval;
  emxInitArray_real_T(&B, 1);
  emxInitArray_real_T(&Latency, 2);

  int reps = 10000;
  clock_t start, end;

  double burst_data[25*25] = {0};
  double rho_data[25*25] = {0};
  double backpressure_data[25] = {0};
  burst_data[1 + 17*25] = 10; rho_data[1 + 17*25] = 0.3; //from 1,0 (position 1) to 2,3 (position 17)
  burst_data[6 + 17*25] = 10; rho_data[6 + 17*25] = 0.3; //from 1,1 (position 6) to 2,3 (position 17)
  burst_data[11 + 17*25] = 10; rho_data[11 + 17*25] = 0.3; //from 1,2 (position 11) to 2,3 (position 17)



  /* Initialize function 'west_fifo_network' input arguments. */

  /* Initialize function input argument 'burst'. */
  burst = c_argInit_UnboundedxUnbounded_r(burst_data);

  /* Initialize function input argument 'rho'. */
  rho = c_argInit_UnboundedxUnbounded_r(rho_data);

  /* Initialize function input argument 'backpressure'. */
  backpressure = argInit_Unboundedx1_real_T(backpressure_data);

  /* Call the entry-point 'west_fifo_network'. */
  start = clock();
  for(; reps > 0; reps--)
    west_fifo_network(5, 5, burst, rho, backpressure, 0, 0, &retval, B, Latency);
  end = clock();

  printf("Time to compute 10000 repetitions: %f seconds \n", (end - start)/(double)CLOCKS_PER_SEC);

  printf("Retval: %f \n", retval);
  printf("Delay at (1,0)->(2,3): %f \n", Latency->data[1 + 17*25]);
  printf("Delay at (1,1)->(2,3): %f \n", Latency->data[6 + 17*25]);
  printf("Delay at (1,2)->(2,3): %f \n", Latency->data[11 + 17*25]);

  emxDestroyArray_real_T(Latency);
  emxDestroyArray_real_T(B);
  emxDestroyArray_real_T(backpressure);
  emxDestroyArray_real_T(rho);
  emxDestroyArray_real_T(burst);
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* Initialize the application.
     You do not need to do this more than one time. */
  west_fifo_network_initialize();
	
  printf("init done\n");

  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_west_fifo_network();

  printf("main done\n");

  /* Terminate the application.
     You do not need to do this more than one time. */
  west_fifo_network_terminate();
  return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
