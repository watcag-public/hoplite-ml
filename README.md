# Learning configuration of Hoplite NoC switches

<img src="readme_data/framework.png" width="50%"><img src="readme_data/hoplite-ml.png" width="20%"><img src="readme_data/switch_types.png" width="30%">


We provide a set of plug and play demos for you to rapidly test our MLE framework for generating Hoplite NoCs for your application. Not only will our MLE framework help you discover NoC switch configurations for popular synthetic (RANDOM and LOCAL) and real world accelerator traces (SpMV and Graph Analytics) but you can also adapt Hoplite NoCs for your own application, by simply providing a list of your application's data flows.

>  *Gurshaant Malik, Ian Elmor Lang, Rodolfo Pellizoni and Nachiket Kapre*

>    <b>Full Paper</b>: [**"Learn the Switches: Evolving FPGA NoCs with
Stall-Free and Backpressure based Routers"**](https://gsmalik.github.io/publications/hoplite-ml_fpl-2020.pdf),

>    <b>Presentation</b>: [**Click Here**](readme_data/presentation.pdf),

>    <b>International Conference on Field-Programmable Logic and Applications, Aug 2020 </b>

## Understanding the MLE framework

The MLE framework works by accepting a user defined trace file that consists of information about the various traffic flows that would be routed on the NoC.
The framework then evolves the NoC configuration specifically for that traffic trace to optimise for a user defined QoR. Over evolutionary steps, the framework
works hand-in-hand with an analysis tool that provides critical information about the routability features (latency, throughput etc) and cost of the
various NoC configurations explored over the course of the exploration.



We now explain the structure, role and interactions of these modules that make up the MLE framework in the list below:

1. <b>[Traffic traces](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/tree/master/bench)</b>

The traffic trace is a collection of packet traces that are to be expected to be routed over the NoC. The NoC will be optimised for a user defined QoR for these traffic traces. We provide various [RANDOM and LOCAL](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/tree/master/bench) traffic traces for users to play with in the repository. In addition to synthetically generated RANDOM and LOCAL traces, we have also made available traffic traces for real-world applications designs for FPGA accelerators such as Sparse matrix vector multiplication ([SpMV](https://dl.acm.org/doi/abs/10.1145/2684746.2689081)) used by many [deep learning kernels](https://arxiv.org/abs/1708.04485) and [Graph analytics](https://snap.stanford.edu/data/). These traffic traces are represented as a DAT file arranged in CSV format consisting of the following columns: <br />

> * <b>sX</b>: The X coordinate of the source PE injecting packets of this traffic trace. <br />
> * <b>sY</b>: The Y orientate of the source PE injecting packets of this traffic trace. <br />
> * <b>dX</b>: The X coordinate of the source PE receiving packets of this traffic trace. <br />
> * <b>dY</b>: The Y coordinate of the source PE receiving packets of this traffic trace. <br />
> * <b>B</b>: The data block size of the packets being injected. This can be overridden by the user with a manual universal burst size by specifying the <b>-B</b> flag when running the framework. More details about the framework's flags are below. <br />
> * <b>R</b>: The regulation rate of regulators at the injection site, moderating the rate of injection of packets at the source. This can be overridden by a manual universal regulation rate by specifying the <b>-R</b> flag when running the learning framework. More details about the framework's flags are below.<br />

2. <b>[QoR Analysis](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/evolveNoC/pyQoR.py)</b>

The QoR analysis tool class, pyQoR, is responsible for quantifying the performance of a particular NoC switch configuration on the provided traffic trace. From a bird's eye perspective, the class accepts as input a NoC configuration (as a binary list; 0 for HopliteBuf and 1 for HopliteBP) and the traffic trace and returns the theoretical upper bounds of worst case routing latency and the associated NoC cost. See the function <b>[config_NoC](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/evolveNoC/pyQoR.py#L112-119)</b> for information about how switches of a particularly sized NoC are configured. See the function <b>[analyze_network](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/evolveNoC/pyQoR.py#L247-255)</b> for details on the steps involved to calculate the QoR and associated cost of the given NoC. We now provide a breakdown of some core functions of the pyQoR class below:

> * <b>setup_Clibs</b>: We have implemented the latency and buffer analysis in Matlab, and converted it to C code using Real Time Workshop. This function setups the appropriate interface for the python implementation to communicate with the C based analysis. <br />
> * <b>setup_pe</b>: The PEs generally refer to the application threads that pump the data into the NoC. Here, the PEs refer to a list of source destination pairs, each configured with data block size and regulation rate from the traffic trace. The generated list is then used by the analysis to quantify performance of a particular NoC configuration on the given QoR.
> * <b>calculate_route_stats</b>: This function infers the analysis to quantify the performance of the NoC on the given traffic trace and returns a list of theoretical worst case routing latency for each of the traffic flows in the traffic trace.
> * <b>analyze_network</b>: This function first configures the PEs, calculates the routing statistics of the NoC on the given traffic trace, calculates the cost of the NoC for the given configuration and finally returns the QoR of the NoC on this trace. 

3. <b>[Learning Framework](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/evolveNoC/models.py)</b>

The learning framework class, models, is the primary learning framework for optimising NoC switch configuration for a particular given traffic trace and QoR. The models class can accommodate different learning methods. Currently, it supports 3 different types of optimization flows: 1. MLE, 2. CMA-ES and 3. RbfOpt, with ability to progressively add more. The user should create an object of this learning models class and choose the method of optimisation. Once the object is created, the user simply needs to invoke the <b>[step](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/evolveNoC/models.py#L14-15)</b> function for a certain number of times until a specific condition is met. This condition could be an upper limit on time spent learning, number of optimization steps, a specific QoR goal or simply wait for the optimization algorithm to tell the user that no more optimization is possible. Below is a breakdown of some core functions of the models class below:

> * <b>init_model</b>: According to the optimization flow selected, this function initializes the optimization flow; setting up the optimization space and constraining the learning to user defined flags and QoRs. You will see that there are other functions by the naming convention "init_XYZ", where XYZ refers to the optimization flow and the function "init_XYZ" refers to that optimization flow's initialization, with the primary call being made by the "init_model" function. Currently, as mentioned before, the repository support the use of "mle", "cma" and "rbf" optimisation flows. If a user wanted to extend this work to include other flows, it would be expected to add an initialization function for that flow in similar spirit.
> * <b>step</b>: The step function performs one step of NoC switch configuration discovery. Although the exact mechanics of this step vary according to the optimization flow, in general, it consists of generating a varying NoC configurations by mutating a base NoC state and evaluating each of these generated NoC configurations on the user defined QoR using the QoR analysis tool class, pyQoR. Having evaluated all generated NoC configurations, a certain top performing percentage of them are then chosen to generate a new base NoC state, effectively capturing the qualities of the top performing NoC configurations in the next base state. Finally, this step returns a boolean flag carrying information if the optimization is complete. Each optimization flow determines its stopping criteria differently. You will also notice that there are other functions of the naming convention "step_XYZ" that are called by this step function according the optimisation flow chosen. Currently, "XYZ" can refer to "mle", "cma" and "rbf". If a user wanted to add another optimisation flow to extend this work, it would be expected to wrap the optimisation/NoC discovery step/single epoch/single evolution with this step function in a similar fashion.
> * <b>compare_QoR_against_vanilla</b>: This function allows the user to compare the current best NoC discovered with the optimization flow against vanilla HopliteBP and HopliteBuf NoCs. You can call this function any time during the learning or once the best NoC configuration has been discovered and learning is complete, use this function to compare its QoR against vanilla variants.

## Using the MLE framework

##### Pre-Requisites:
* [CMA](https://pypi.org/project/cma/)
* [RbfOpt](https://github.com/coin-or/rbfopt)
* [Numpy](https://numpy.org/)
* [Pandas](https://pandas.pydata.org/)


##### Get up and running with the framework:
We now provide detailed steps for using the MLE framework to learn NoC switch configurations.

1. You will need to generate the C analysis files for the pyQoR class to use. You only need to do this once unless those files are deleted or you modify some part of the analysis. In the home folder of the repository, run the following command in the shell of your choice:

```shell
make sharedlib
```

2. Having generated the analysis files, you will need to point the ctypes API to these files by exporting its path. Remember, you might need to do this every time you infer a new shell if your environment variables are reset every time a new shell is invoked. In the home folder of the repository, run the following command in the shell of your choice:

```shell
export LD_LIBRARY_PATH=codegen/exe/west_fifo_network:$LD_LIBRARY_PATH
```

3. Now you are ready to learn the NoC configuration for a traffic trace of choosing. If you have created your own traffic trace, either synthetically or captured from your application, make sure it has been added to the repository. 

4. Run the following command to start optimising your NoC by specifying the traffic trace path relative to the home directory of the repository (PATH_TRAFFIC_TRACE), the size of the NoC (SIZE_NoC) ,the optimization type between fifo,bp,mle,cma,rbf (OPT_CHOICE) and the population size of NoC candidates (POP_SIZE) generated during a single learning step.

```shell
python main.py -f PATH_TRAFFIC_TRACE -N SIZE_NoC -ML OPT_CHOICE -P POP_SIZE
```

5. We now provide the console output of running a sample end-to-end NoC learning flow. Here we are learning NoC configuration for a traffic trace (-f) called "random_4x4-85.dat" for a NoC of size (N) 4x4 with a data block size (B) of 4, universal regulation rate (R) of 0.15. We use the optimization flow (ML) of mle with a population size (P) of 1000. We also enable comparison against base (cab) and set an upper limit in optimization time (T) to 30 seconds:

```shell
$ python main.py -f bench/random_4x4-85.dat -N 4 -B 4 -R 0.15 -ML mle -P 1000 -T 30

HopliteBP NoC QoR: 1016064.0
HopliteBuf NoC QoR: 602784.0
MLE generated NoC QoR: 498400.0
```

##### Understanding the various Framework flags

We now delve deeper into the various flags available to the user to control the framework's behaviour in [main.py](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/blob/master/main.py). Not all of these flags need to be explicitly specified before launching a learning run. You are also free to add more flags to control or constrain any additional features you might add to extend this work.

> * <b>-N</b>: Size of the NxN NoC. Integer type. User is required to specify this. <br />
> * <b>-f</b>: A file containing a list of flows. Path relative to home directory of the repository. User is required to specify this explicitly. <br />
> * <b>-B</b>: Global data block size that overrides the traffic trace's values. <br />
> * <b>-R</b>: Global regulation rate that overrides the traffic trace's values. <br />
> * <b>-ML</b>: The type of optimization flow to be used. If learning the NoC switches, the current options are "mle", "rbf", "cma". If you want to infer a vanilla HopliteBP NoC and measure QoR, you can set this as "bp". For HopliteBuf, you can set this as "fifo". User is required to specify this explicitly.<br />
> * <b>-T</b>: The maximum amount of time the learning should take place, in seconds. If the learning has not already quit by itself or some other termination condition has not been triggered, the optimization is forcefully closed once the specified amount of time has been spent in learning. The best NoC configuration found so far is used as the final solution. Defaults to 60s if not specified.<br />
> * <b>-P</b>: The population size used in optimisation flows. This controls the number of candidate NoC configurations generated during a single learning step. Typically, a greater population size can result in a better solution. But this comes at the expense of increased learning times because each candidate NoC configuration of the population has to be analyzed and its QoR measured. Not applicable if -ML type is "fifo" or "bp". User is required to specify this explicitly.<br />
> * <b>-W</b>: Write out intermediate results explored. Allow debugging the search space explored by the optimizer. Defaults to 0 (False) if not specified.<br />
> * <b>-PF</b>: Record per-flow latency for each optimizer iteration. Allow debugging the search space explored by the optimizer. Defaults to 0 (False) if not specified.<br />
> * <b>-O<b>: Operating mode of the optimiser. Options are "sharedlib, "octave", "matlabcsv". Defaults to "sharelib" if not specified.<br />
> * <b>-pss</b>: Choose whether to prune the search space to accelerate learning. If selected, the search space would be pruned to only optimise over turning hoplite routers. Non-turning routers will operate in fifo mode.<br />
> * <b>-FF</b>: The fitness function used by the optimizer. Current options are "wclatency-cost", "rate-weighted-wclatency-cost" and "latency-only". Defaults to "wclatency-cost"<br />
> * <b>-mil</b>: Whether you want to minimize difference in sum of ideal latencies and actual latencies. If enabled, the optimizer will aim to minimize the difference between sum of latencies of flows and ideal sum of latencies of flows.<br />

#### Deploying MLE generated NoCs on FPGAs
We also design a unified HopliteBuf+BP switch that can be configured to work in either FIFO or BP mode on the fly. Apart from traditional use for connecting FPGA sub-systems, this switch can also be fabricated and deployed in ASICs. The MLE framework can run on a separate host machine or a tiny processor on-chip, with switch reconfiguration happening in real time to respond to evolving routing conditions.

To capture this idea, we have implemented and verified the entire framework on a Pynq Z1. We implemented the MLE optimization tool on the Cortex A9 processor (PS) to configure the FPGA NoC switches dynamically, while realizing the NoC itself on the FPGA. For the unified NoC, the software layer on the ARM configures the switches dynamically over an AXI bus, depending on the application requirements, without the need for complete bitstream reconfiguration. For a 6x6 network, the runtime of the MLE optimization on the PS is 9-60s, and the reconfiguration of the switches instantiated in the PL takes 850ms.


## FAQs
<b>

* I want to learn a bit more about this work. Apart from the paper, is there any source I can use to get upto speed with the prior work?
> Yes! Thank you for taking interest to learn more! You can always read the original [Hoplite Paper](https://nachiket.github.io/publications/hoplite_trets2017.pdf) for the deflection routed NoC. Regarding the HopliteBuf and BP variants used in this work, you can refer to the [HopliteBuf journal](https://nachiket.github.io/publications/hoplitebuf_trets2020.pdf). 

* I want to learn a bit more about the concept of Evolutionary based methods for other applications. Is there any other source that I can refer to?
> Yes! You can refer to video below for more information. Also, you can refer to this [paper](https://nachiket.github.io/publications/part-systolic_fpt-2019.pdf) which used evolutionary learning/Neuro-Evolution to partition separable neural networks for efficient mapping to custom systolic arrays
[![NeuroEvolution Primer](http://img.youtube.com/vi/zlxxKsfgDA8/0.jpg)](http://www.youtube.com/watch?v=zlxxKsfgDA8 "NeuroEvolution Primer")

* Is there a video presentation of this work available?
> Yes! It is embedded below <br />
[![Hoplite-ML](http://img.youtube.com/vi/RqLlSyL5y4A/0.jpg)](http://www.youtube.com/watch?v=RqLlSyL5y4A "Hoplite-ML")

* I want to learn the NoC configuration for a custom application that I have designed. Can I do that? If so, how do I go about doing that?
> First, you can definitely learn NoC configuration for your custom application using our framework. Akin to synthetic and real traffic traces available in the [bench](https://git.uwaterloo.ca/watcag-public/hoplite-ml/-/tree/master/bench) folder, you simply need to define a traffic trace file with similar structure as the others. To fill up your traffic trace files with information related to individual traffic flows, you can simulate your application for a specified amount of time and capture necessary details (src-dst pairs, regulation rate, data block). If you would prefer that the NoC responds to dynamically changing traffic conditions when deployed on a FPGA in a real time, you can consider implementing the learning framework on a co-host/on-chip processor and unified HopliteBuf+BP switches on the FPGA. You can periodically share information about traffic flows between the FPGA and the framework, which would allow the framework to configure these unified switches in either mode on a per-switch basis, thus allowing you to respond to the changing routing conditions of your applications.
</b>

### License
This tool is distributed under MIT license.

Copyright (c) 2019 Gurshaant Malik, Ian Elmor Lang, Rodolfo Pellizoni and Nachiket Kapre

<div style="text-align: justify;"> 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
<br><br>
</div>


<div style="text-align: justify;"> 
<b>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</b>
<br><br>
</div>


<div style="text-align: justify;"> 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 </div>
