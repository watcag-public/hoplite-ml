import numpy as np
import csv
import sys

if len(sys.argv) < 3:
    exit(1)

bench = sys.argv[1]
sz = int(sys.argv[2])
sz_sq = sz * sz

with open('matrix_{}_{}.dat'.format(bench, sz_sq), 'r') as data_file:
    data_rows = [list(map(int, row)) for row in csv.reader(data_file)]

data_rows.sort()
data_rows = [row[1:] for row in data_rows]

max_dat = 0

for i, row in enumerate(data_rows):
    for j, dat in enumerate(row):
        if i == j or dat == 0:
            continue
        max_dat = max(max_dat, dat)

with open('{}_{}x{}.dat'.format(bench, sz, sz), 'w+') as write_file:

    write_file.write('sX,sY,dX,dY,B,R\n')

    for i, row in enumerate(data_rows):
        dX, dY = i // sz, i % sz
        for j, dat in enumerate(row):
            if i == j or dat == 0:
                continue
            sX, sY = j // sz, j % sz
            write_file.write('{},{},{},{},1,{:1.6f}\n'.format(sX, sY, dX, dY, dat / max_dat))