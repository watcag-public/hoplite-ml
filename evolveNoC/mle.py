import numpy as np
import sys

class pymle:
    def __init__(self,size=16,population=100,p=0.5,verbose=False,fw=sys.stdout):
        self.size = size
        self.population = population
        self.p = np.array([p for _ in range(size)])
        self.best = np.zeros(size)
        self.best_fitness = sys.float_info.max
        self.verbose = verbose
        self.fw=fw

    def generate(self):
        self.samples = np.zeros((self.size,self.population))
        for index in range(self.size):
            self.samples[index] = np.random.binomial(1,self.p[index],(1,self.population))
            self.samples[index][0] = 0
            self.samples[index][1] = 1
        self.samples = self.samples.T
   
    def update(self,fitness):
        self.fitness = np.array(fitness)
        assert np.size(fitness) == self.population
        top_sel = int(0.25*self.population)
        self.samples_sorted = self.samples[np.argsort(self.fitness)]
        self.fittest = self.samples_sorted[:top_sel]
        self.fitness_sorted = self.fitness[np.argsort(self.fitness)]
        if self.fitness_sorted[0] <= self.best_fitness:
            self.best = self.fittest[0]
            self.best_fitness = self.fitness_sorted[0]
        self.p = np.mean(self.fittest,axis=0)