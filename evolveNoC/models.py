import numpy as np


class model:
    def __init__(self, qor_tool, args):
        self.qor_tool = qor_tool
        self.setup_args(args)
        self.init_model(qor_tool)

    def init_model(self, qor_tool):
        assert self.ml_type in ("mle", "cma", "rbf", "bp", "fifo"), "Unknown ML type"
        eval("self.init_" + str(self.ml_type) + "()")

    def step(self, iteration):
        return eval("self.step_" + str(self.ml_type) + "(" + str(iteration) + ")")

    def step_mle(self, iteration):
        self.alg.generate()
        self.alg.update(
            [
                self.qor_tool.analyze_network(
                    x=x,
                    mode=self.ml_type,
                    prune_search_space=self.prune_search_space,
                    iteration=iteration,
                    verbose=self.writeMode,
                )
                for x in self.alg.samples
            ]
        )
        self.best_sol = self.alg.best
        self.best_QoR = self.qor_tool.analyze_network(
            x=self.best_sol,
            mode=self.ml_type,
            prune_search_space=self.prune_search_space,
            iteration=iteration,
            verbose=self.writeMode,
            write=self.writeMode,
            write_pf=self.recordPFLatency,
        )
        return ((self.alg.p == 0.0) | (self.alg.p == 1.0)).all()

    def step_cma(self, iteration):
        solutions = self.alg.ask()
        results = [
            self.qor_tool.analyze_network(
                x=x,
                mode=self.ml_type,
                prune_search_space=self.prune_search_space,
                iteration=iteration,
                verbose=self.writeMode,
            )
            for x in solutions
        ]
        self.alg.tell(solutions, results)
        self.best_sol = self.alg.result[0]
        self.best_QoR = self.qor_tool.analyze_network(
            x=self.best_sol,
            mode=self.ml_type,
            prune_search_space=self.prune_search_space,
            iteration=iteration,
            verbose=self.writeMode,
            write=self.writeMode,
            write_pf=self.recordPFLatency,
        )
        self.alg.disp()
        return self.alg.stop()
    
    def step_rbf(self,iteration):
        _, x, itercount, _, _ = self.alg.optimize()
        self.best_sol = x
        self.best_QoR = self.qor_tool.analyze_network(
            x=self.best_sol,
            mode=self.ml_type,
            prune_search_space=self.prune_search_space,
            iteration=itercount,
            verbose=self.writeMode,
            write=self.writeMode,
            write_pf=self.recordPFLatency,
        )
        return True

    def step_fifo(self, iteration):
        self.fifo_QoR = self.qor_tool.analyze_network(
            x=np.array([0] * self.qor_tool.size * self.qor_tool.size),
            mode="fifo",
            prune_search_space=0,
            iteration=iteration,
            verbose=False,
        )
        if self.ml_type == "fifo":
            print("HopliteBuf NoC QoR:", self.fifo_QoR)
        return True

    def step_bp(self, iteration):
        self.bp_QoR = self.qor_tool.analyze_network(
            x=np.array([1] * self.qor_tool.size * self.qor_tool.size),
            mode="bp",
            prune_search_space=0,
            iteration=iteration,
            verbose=False,
        )
        if self.ml_type == "bp":
            print("HopliteBP NoC QoR:", self.bp_QoR)
        return True

    def compare_QoR_against_vanilla(self, iteration):
        self.step_fifo(iteration)
        self.step_bp(iteration)
        print("HopliteBP NoC QoR:", self.bp_QoR)
        print("HopliteBuf NoC QoR:", self.fifo_QoR)
        print(self.ml_type.upper(),"generated NoC QoR:", self.best_QoR)
        self.best_sol = (
            self.best_sol
            if self.best_QoR <= self.fifo_QoR and self.best_QoR <= self.bp_QoR
            else np.array([0] * self.qor_tool.size * self.qor_tool.size)
            if self.fifo_QoR <= self.bp_QoR
            else np.array([1] * self.qor_tool.size * self.qor_tool.size)
        )

        self.best_QoR = self.qor_tool.analyze_network(
            x=self.best_sol,
            mode=self.ml_type,
            prune_search_space=(
                self.prune_search_space
                if self.best_QoR <= self.fifo_QoR and self.best_QoR <= self.bp_QoR
                else 0
            ),
            iteration=iteration,
            verbose=False,
            write=self.writeMode,
            write_pf=False,
        )

    def init_mle(self):
        from evolveNoC import mle

        if self.prune_search_space == 1:
            self.alg = mle.pymle(
                size=self.qor_tool.pruned_size,
                population=self.population,
                p=0.5,
                verbose=self.writeMode,
            )
        else:
            self.alg = mle.pymle(
                size=self.qor_tool.size * self.qor_tool.size,
                population=self.population,
                p=0.5,
                verbose=self.writeMode,
            )

    def init_rbf(self):
        import rbfopt

        settings = rbfopt.RbfoptSettings(
            minlp_solver_path="./bonmin", max_clock_time=self.max_time
        )
        if self.prune_search_space == 1:
            bb = rbfopt.RbfoptUserBlackBox(
                self.qor_tool.pruned_size,
                np.array([0] * self.qor_tool.pruned_size),
                np.array([1] * self.qor_tool.pruned_size),
                np.array(["I" for _ in range(self.qor_tool.pruned_size)]),
                self.qor_tool.analyze_network,
            )
        else:
            bb = rbfopt.RbfoptUserBlackBox(
                self.qor_tool.size,
                np.array([0] * self.qor_tool.size),
                np.array([1] * self.qor_tool.size),
                np.array(["I" for _ in range(self.qor_tool.size)]),
                self.analyze_rbf,
            )
        self.alg = rbfopt.RbfoptAlgorithm(settings, bb)

    def analyze_rbf(self, x):
        return self.qor_tool.analyze_network(
            x=x,
            mode=self.ml_type,
            prune_search_space=self.prune_search_space,
            iteration=0,
            verbose=self.writeMode,
        )

    def init_cma(self):
        import cma

        if self.prune_search_space == 1:
            self.alg = cma.CMAEvolutionStrategy(
                (self.qor_tool.pruned_size) * [0], 1, {"popsize": self.population}
            )
        else:
            self.alg = cma.CMAEvolutionStrategy(
                (self.qor_tool.size * self.qor_tool.size) * [0],
                1,
                {"popsize": self.population},
            )

    def init_fifo(self):
        pass

    def init_bp(self):
        pass

    # FLAG: remove unwanted from here
    def setup_args(self, args):
        self.ml_type = args.ML
        self.population = args.P
        self.writeMode = args.W
        self.recordPFLatency = args.PF
        self.prune_search_space = args.pss
        self.max_time = args.T
