import pandas as pd
import numpy as np

def encode (flow_file, size):
    turn_indices = []
    df = pd.read_csv(flow_file, sep=',\s+', engine='python')
    length = len(df.index)
    for index in range(length):
        dx = int(df['dX'][index])
        sy = int(df['sY'][index])
        assert dx < size, "Dest. PE's X index should be less than column size \
        of NoC"
        assert sy < size, "Source PE's Y index should be less than row size \
        of NoC"
        turn_index = (sy*size) + dx
        if int(turn_index) not in turn_indices:
            turn_indices.append(int(turn_index))
    return np.array(turn_indices)

def decode(turn_indices, sample, size):
    assert len(turn_indices) == len(sample), "Length of 'turn_indices' and \
    'sample' does not match."
    encoded_space_size = len(turn_indices)
    decoded_space = np.zeros((size*size,), dtype=int)
    
    for index in range(encoded_space_size):
        decoded_space[turn_indices[index]] = sample[index]

    return decoded_space

