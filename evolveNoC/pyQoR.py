#!/usr/bin/env python
""" pyQoR.py: QoR analyis accompanying Hoplite-ML

This class represents the QoR analysis tool that is each time 

Auhtors: Gurshaant Malik, Ian Elmor Land, Rodolfo Pellizoni, Nachiket Kapre
"""

import subprocess as CMD
from ctypes import cdll, c_int
import numpy as np
import pandas as pd
from evolveNoC import prune
import sys
import time


class pyQoR:
    def __init__(self, args):
        self.setup_args(args)

        self.df = pd.read_csv(self.file_name, sep=",\s+", engine="python")
        self.f1 = open("./result.csv", "w+")
        self.f2 = open("./evo.csv", "w+")

        self.df["mil"] = (
            (1 / self.df["R"])
            + abs(self.df["sX"] - self.df["dX"])
            + abs(self.df["sY"] - self.df["dY"])
        )
        self.setup_Clibs()
        self.setup_pe()
        self.setup_pruned_space()
        self.reset_Clibs()

    # go through each of them, search for them and if not found, remove them
    def setup_args(self, args):
        self.file_name = args.f
        self.size = args.N
        self.burst_user = args.B
        self.rate_user = args.R
        self.operating_mode = args.O
        self.fitness_function = args.FF
        self.minimise_ideal_latency = args.mil

    def setup_Clibs(self):

        if self.operating_mode == "sharedlib":
            self.lib = cdll.LoadLibrary("libwest_fifo_network.so")
            self.lib.main_west_fifo_network.argtypes = [c_int]
            self.lib.main_west_fifo_network_initialize.argtypes = [c_int]
            self.lib.main_west_fifo_network_terminate.argtypes = [c_int]
            self.lib.main_west_fifo_network_with_args.argtypes = [
                c_int,
                np.ctypeslib.ndpointer(dtype=np.double),
                np.ctypeslib.ndpointer(dtype=np.double),
                np.ctypeslib.ndpointer(dtype=np.double),
            ]
            self.lib.main_west_fifo_network_with_args.restype = c_int

    def reset_Clibs(self):
        if self.operating_mode == "sharedlib":
            self.lib.get_buffer.restype = np.ctypeslib.ndpointer(
                dtype=np.double, shape=(self.size * self.size,)
            )
            self.lib.get_wclatency.restype = np.ctypeslib.ndpointer(
                dtype=np.double, shape=(self.size * self.size * self.size * self.size)
            )
            self.lib.west_fifo_network_initialize()

    def setup_pe(self):
        self.pe_bm = np.zeros(
            (self.size * self.size, self.size * self.size), dtype=np.double
        )
        self.pe_rm = np.zeros(
            (self.size * self.size, self.size * self.size), dtype=np.double
        )
        self.length = len(self.df.index)

        for k in range(self.length):
            sx = int(self.df["sX"][k])
            sy = int(self.df["sY"][k])
            dx = int(self.df["dX"][k])
            dy = int(self.df["dY"][k])
            if self.burst_user is None:
                self.burst = np.double(self.df["B"][k])
            else:
                self.burst = np.double(self.burst_user)
            if self.rate_user is None:
                self.rate = np.double(self.df["R"][k])
            else:
                self.rate = self.rate_user

            self.pe_bm[sy * self.size + sx][dy * self.size + dx] = self.burst
            self.pe_rm[sy * self.size + sx][dy * self.size + dx] = self.rate

            if self.operating_mode == "octave" or self.operating_mode == "matlabcsv":
                np.savetxt("pe_rm_0.csv", self.pe_rm, fmt="%f", delimiter=",")
                np.savetxt("pe_bm_0.csv", self.pe_bm, fmt="%d", delimiter=",")

    def setup_pruned_space(self):
        self.turn_router_indices = prune.encode(self.file_name, self.size)
        self.pruned_size = len(self.turn_router_indices)

    def config_NoC(self, x):
        self.sw_bp = np.zeros(self.size * self.size, dtype=np.double)

        for i, el in enumerate(x):
            if el > 0:
                self.sw_bp[i] = 1
            else:
                self.sw_bp[i] = 0

    def calculate_route_stats(self):
        if self.operating_mode == "octave" or self.operating_mode == "matlabcsv":
            np.savetxt("sw_bp_0.csv", self.sw_bp, fmt="%d", delimiter=",")

        # print "Calling Octave script to run analysis"
        if self.operating_mode == "octave":
            CMD.getoutput(
                "cd octave; cp ../*.csv .; octave-cli top_extract_all_backpressure.m %d %d %d; cp *.csv ..;"
                % (
                    self.size,
                    0,
                    0,
                ) 
            )
        elif self.operating_mode == "matlabcsv":
            # make sure library recompiled correctly
            # TS_BL assumed to be 0, suffix hardcoded as 0
            CMD.getoutput("./codegen/exe/west_fifo_network/test_C %d " % (self.size))
        else:
            # TS_BL assumed to be 0, suffix hardcoded as 0
            self.stable = self.lib.main_west_fifo_network_with_args(
                self.size,
                np.ravel(self.pe_bm, order="F"),
                np.ravel(self.pe_rm, order="F"),
                self.sw_bp,
            )
            self.fread_burst = self.lib.get_buffer()
            self.fread_total = self.lib.get_wclatency()

        if self.operating_mode == "octave" or self.operating_mode == "matlabcsv":
            self.fread_total = np.genfromtxt("total_0.csv", delimiter=",")
            self.fread_burst = np.genfromtxt("buffer_0.csv", delimiter=",")
            self.stable = int(np.max(np.genfromtxt("stable_0.csv", delimiter=",")))

        self.max_fifo_size = np.max(self.fread_burst)
        self.sum_fifo_size = np.sum(self.fread_burst)

        if self.minimise_ideal_latency:
            self.wclatency = np.sum(self.fread_total) - self.df["mil"].sum()
        else:
            self.wclatency = np.max(self.fread_total)

    def calculate_cost_stats(self):
        COST_FIFO_AND_SHADOW = 247
        COST_FIFO_ONLY = 161
        COST_SHADOW_ONLY = 189

        self.cost = 0

        if self.max_fifo_size <= 32:
            for i in range(self.size):
                cur_sum = np.sum(self.sw_bp[i * self.size : (i + 1) * self.size])
                if cur_sum == 0:
                    self.cost += COST_FIFO_ONLY * self.size
                else:
                    self.cost += (
                        cur_sum * COST_SHADOW_ONLY
                        + (self.size - cur_sum) * COST_FIFO_AND_SHADOW
                    )
        else:
            self.cost = 1000000000
            self.stable = 0

    def write_results(self, x, mode, prune_search_space, iteration, verbose, write, write_pf):
        if write:
            fw = self.f1
        else:
            fw = sys.stdout
        
        if verbose:
            print(repr(x).replace("\n", ""), file=fw)

        if write_pf:
            to_print = ",".join(
                map(
                    str,
                    [self.file_name, mode, iteration, self.stable]
                    + list(self.fread_total),
                )
            )
            print(to_print, file=self.f2)

        self.network_unstable = (
            self.stable == 0
            or np.where(np.isnan(self.fread_burst))[0].shape[0]
            or np.where(np.isnan(self.fread_total))[0].shape[0]
        )
        if verbose:
            print(
                (
                    self.file_name,
                    prune_search_space,
                    len(self.turn_router_indices),
                    self.size,
                    self.burst,
                    self.rate,
                    0,
                    mode,
                    self.stable,
                    self.max_fifo_size if not (self.network_unstable) else 0,
                    self.sum_fifo_size if not (self.network_unstable) else 0,
                    self.wclatency if not (self.network_unstable) else "",
                    self.cost if not (self.network_unstable) else "",
                    time.time(),
                ),
                file=fw,
            )
            print(repr(self.sw_bp).replace("\n", ""), file=fw)
            fw.flush()

    def calculate_QoR(self):
        if self.fitness_function == "rate-weighted-wclatency-cost":
            QoR = self.cost * np.dot(np.ravel(self.pe_rm, order="F"), self.fread_total)
        elif self.fitness_function == "wclatency-cost":
            QoR = self.cost * self.wclatency
        elif self.fitness_function == "latency-only":
            QoR = self.wclatency

        return float(QoR)

    def prune_NoC_search_space(self, x, prune_search_space):
        if prune_search_space == 1:
            x = prune.decode(self.turn_router_indices, x, self.size)
        return x

    def analyze_network(self, x, mode, prune_search_space, iteration, verbose=False, write=False, write_pf=False):

        x = self.prune_NoC_search_space(x, prune_search_space)
        self.config_NoC(x)
        self.calculate_route_stats()
        self.calculate_cost_stats()
        self.write_results(x, mode, prune_search_space, iteration, verbose, write, write_pf)
        return self.calculate_QoR()

    def close_lib(self):
        if self.operating_mode == "sharedlib":
            self.lib.west_fifo_network_terminate()        
